# This file is licensed under the MIT License.
# 
# Copyright (c) 2015 by The DiSTI Corporation.
# 
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
# 
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
# 
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.

""" Launches the specified exe on the local machine """

import subprocess
import threading
import Common.EditorThreadQueueUtil as editor
import os
import Common.Run.Runner
import platform

def TypeId():
    return "LocalExe"
    
def Run( runner ):
    def worker():
        if 'Linux' == platform.system():
            proc = subprocess.Popen( runner.command, cwd=os.path.dirname(runner.command))
        else:
            proc = subprocess.Popen( runner.command, cwd=os.path.dirname(runner.command), creationflags=subprocess.CREATE_NEW_PROCESS_GROUP)
               
        Common.Run.Runner.existingRunProcessByType[ runner.typeOfRun ] = proc
        
        if None==proc.poll():
            editor.LogInfo("Process started.  Any output should be available in the GL Studio Console.")               
        proc.wait()        
        editor.LogInfo("Process exited normally")        
        print("Process exited normally")        
            
    t = threading.Thread(target=worker)
    t.daemon = True
    t.start()
    
