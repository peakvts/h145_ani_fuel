# This file is licensed under the MIT License.
# 
# Copyright (c) 2015 by The DiSTI Corporation.
# 
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
# 
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
# 
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.

""" Identifies all exported files from a GL Studio document for the Desktop C++ generator """

import Editor
import EditorLog
import Document
import os
import ComponentRef
import json

def ExpectedGenerator():
    return "Desktop C++"
    
def Collect( doc, collection, alreadyVisitedFiles ):
    """ Populates the dictionary with files that are output from this document from the current generator.
    """
    if doc.GetCodeOutputPath():
        EditorLog.PrintError( "Code Output Path expected to be blank, but found: '" + doc.GetCodeOutputPath() + "' in " + doc.GetFilename() + 
                              "\nOpen Generation tab and change Code Output Path to blank.")
        return   
        
    codePath = Editor.GetCodePathForCurrentGenerator( doc )

    if doc.GetCodeGenerationMode() == Document.Document.CODE_GENERATION_MODE_STANDALONE:
        collection['HEADER'].add( os.path.abspath(codePath + "/" + doc.GetStandaloneHeaderFile()) )
        collection['SOURCE'].add( os.path.abspath(codePath + "/" + doc.GetStandaloneSourceFile()) )
        if doc.IsGenerateMainInSeparateFile():
            collection['SOURCE'].add( os.path.abspath(codePath + "/" + doc.GetStandaloneMainFile()) )
                            
    else:
        collection['HEADER'].add( os.path.abspath(codePath + "/" + doc.GetComponentHeaderFile()) )
        collection['SOURCE'].add( os.path.abspath(codePath + "/" + doc.GetComponentSourceFile()) )
            
    alreadyVisitedFiles.add( doc.GetFilename() )
    
    #############################
    # The FindInDocument() below changes the document selection, so here we save the current selection,
    # and then later put it back when we are done.
    originalSelection = doc.GetSelectedObjects()
    doc.ClearSelections()
    selectedString = ""
    
    doc.FindInDocument(None, "ComponentBase", 
                       None, None,
                       1,
                       True,
                       False,
                       False)
    selectedObjectArray = doc.GetSelectedObjects()
    for i in range(selectedObjectArray.GetCount()):
        obj = ComponentRef.CastToComponentRef( selectedObjectArray.GetObjectByIndex(i) )
        if obj.UsingLiveComponent(): # Ignore live components (DLL/so)
            continue
        selectedString += obj.GetName() + " ";
        referencedFile  = obj.GetComponentFilePath()
        bareFileName    = os.path.split(referencedFile)[1]
        if not (bareFileName in alreadyVisitedFiles) and referencedFile:
            newDoc = Editor.OpenDocument( referencedFile )
            Collect( newDoc, collection, alreadyVisitedFiles )
            doc.MakeCurrentDocument()
        
    doc.ClearSelections()
    # Put the selection back where we found it
    doc.SelectObjects(originalSelection);  

