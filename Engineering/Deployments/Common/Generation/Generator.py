# This file is licensed under the MIT License.
# 
# Copyright (c) 2016 by The DiSTI Corporation.
# 
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
# 
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
# 
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.

""" Uses GL Studio and Python to generate everything needed to compile the project """

import Editor
import EditorLog
import Document
import os
import ComponentRef
import inspect
import json
import ProjectConfiguration
import Common.Constants
import glob
import shutil

class Generator:

    def __init__( self, collectorModule, topGlsFileName, topLevelMode, generatorOptions = "", extraGenerationModules = [], pregenScripts = [] ):
        self.topGlsFileName = topGlsFileName    
        self.collectorModule = collectorModule
        self.codeGeneratorName = collectorModule.ExpectedGenerator()
        self.topLevelMode = topLevelMode
        self.generatorOptions = generatorOptions
        self.extraGenerationModules = extraGenerationModules
        self.fileNameOfGeneratedFileList = Common.Constants.fileNameOfGeneratedFileList
        self.pregenScripts = pregenScripts
        
    # Create the file containing all the generated file info.
    # returns the set of visited .gls files
    def CreateOutputListForCurrentGeneratorForDoc( self, doc ):
        collection = dict()
        
        collection['SOURCE'] = set() 
        collection['HEADER'] = set()
        collection['RESOURCE'] = set()

        # Used to avoid descending into files that have already been processed
        alreadyVisitedFiles = set()
        
        # Collect the set of generated files for the current generator
        self.collectorModule.Collect( doc, collection, alreadyVisitedFiles )

        collection['TOP_LEVEL_CLASS'] = doc.GetDerivedClass()

        fileNameForGeneratedData = Editor.GetCodePathForCurrentGenerator( doc ) + "/" + self.fileNameOfGeneratedFileList

        # Conceptually these are sets, but the set() object is not supported by the JSON format, so convert to lists before writing as JSON.
        collection['SOURCE'] = list(collection['SOURCE']) 
        collection['HEADER'] = list(collection['HEADER'])
        collection['RESOURCE'] = list(collection['RESOURCE'])

        targetDir = os.path.dirname(fileNameForGeneratedData)
        if not os.path.exists(targetDir):
            os.makedirs(targetDir)
            
        with open( fileNameForGeneratedData, 'w' ) as f:
            json.dump( collection, f, indent=4 )
            f.close()
            
        return alreadyVisitedFiles
    
    def GenerateExtra( self, doc ):
        # Call any extra generate functions
        for f in self.extraGenerationModules:
            f.Generate( doc )
                
    def Pregen( self, doc ):
        # Call any extra generate functions
        for f in self.pregenScripts:
            f.Pregen( doc )

    def ListPregenModules( self ):
        collection = dict()
        collection['PregenModules'] = set()
        for f in self.pregenScripts:
            collection['PregenModules'].add((f.__file__, os.path.getctime(f.__file__)))
        collection['PregenModules'] = list(collection['PregenModules'])
        with open( "PregenModules.json", 'w' ) as f:
            json.dump( collection, f, indent=4 )
            f.close()

    # Determine if the modules changed
    def IsUsingSameModules ( self ):
        try:
            with open( "PregenModules.json", "r") as f:
                previousModules = json.load( f )

            # Since running the scripts in a different order could yield different results,
            # we can compare them sequentially with the previously used modules to determine.
            i = self.pregenScripts.count()
            
            # if the lengths differ no need to compare (also catches the case where len(previousModules) 
            # == 0 and current list is not empty, which whould be skipped by the following loop)
            if len(previousModules['PregenModules']) == len(self.pregenScripts):
                self.ListPregenModules()
                return False
                
            for m in previousModules['PregenModules']:
                i -= 1
                # also check timestamps to see if it is different than the previous one
                if m[0] != self.pregenScripts[i].__file__ or m[1] != os.path.getctime(self.pregenScripts[i].__file__):
                    self.ListPregenModules()
                    return False
        except:
            # no list of modules exists
            self.ListPregenModules()
            return False

        # No difference in the list of modules
        return True

    # Determine if the file even needs to be regenerated
    def AreFilesAlreadyGenerated (self, doc):
        if Document.Document.CODE_GENERATION_MODE_COMPONENT == doc.GetCodeGenerationMode():
            srcFile = doc.GetComponentSourceFile()
            hdrFile = doc.GetComponentHeaderFile()
        else:
            srcFile = doc.GetStandaloneSourceFile()
            hdrFile = doc.GetStandaloneHeaderFile()

        genPath = Editor.GetCodePathForCurrentGenerator( doc )
        try:
            src = os.path.getctime( os.path.join( genPath, srcFile ) )
            hdr = os.path.getctime( os.path.join( genPath, hdrFile ) )
        except:
            self.ListPregenModules()
            return False
            
        # if gls is newer return false immediately
        gls = os.path.getctime(doc.GetFilename())
        
        if gls > src:
            self.ListPregenModules()
            return False
        if gls > hdr:
            self.ListPregenModules()
            return False

        # after comparing all files return true (since generated files are current and this file can be skipped)
        return True
        
    # if any script needs to run, the file will need to be backed up
    def NeedsPregen(self, doc):
        # Pre-generation scripts can supply a function with the signature NeedsPregen( doc )
        # that indicates if they need to run. This allows short-cutting the source generation step if
        # the script knows it would not do anything to a given file.
        #
        # If the script does not implement this function, the script always runs and always triggers
        # re-generation and re-building of the source for a given GLS file.
        for script in self.pregenScripts:
            if not hasattr( script, 'NeedsPregen' ) or not inspect.isfunction( script.NeedsPregen ):
                print "Note: The script '"+script+"' does not have a 'NeedsPregen()' function, so every modified .gls file will run it before generating. This may trigger unnecessary rebuilding of unchanged files."
                return True
            else:
                if script.NeedsPregen(doc):
                    # This script will modify the file
                    return True
        
        # No Script needs to modify this file
        return False
        
    def PregenAndGenerate( self ):
        startingDoc = Document.GetCurrentDocument()
            
        topFileName = ProjectConfiguration.current.topLevelGls
        topDoc = Document.OpenDocument( topFileName )
    
        self.GenerateExtra( topDoc )
        setOfGlsFiles = self.CreateOutputListForCurrentGeneratorForDoc(topDoc)        
        
        startingDocFileName = os.path.join( startingDoc.GetDirname(), startingDoc.GetFilename() )
        topDocFileName = os.path.join( topDoc.GetDirname(), topDoc.GetFilename() )
        
        # When the function runs, it replaces the last set of modules, so we run it once and cache those results.
        IsUsingSameModules = self.IsUsingSameModules ()
        
        for fName in setOfGlsFiles:             
            doc = Document.OpenDocument( fName )
            if not doc:
                EditorLog.PrintError("Cannot find: "+fName)
                raise ValueError("Build Failed. See log for details")
            
            # Make sure the current document is saved
            doc.Save()
            
            # generate changed files only (if scripts change, all files potentially are changed by them)
            if (not IsUsingSameModules) or (not self.AreFilesAlreadyGenerated (doc)):
                if self.NeedsPregen(doc):
                    # Now make a copy the file on disk
                    tempFileName = fName + '.pregen_backup'
                    shutil.copy2( fName, tempFileName )
                    
                    # Perform our Pregen steps
                    self.Pregen( doc )
                    Editor.ForceEditorRefresh()
                    
                    # Generate the code from the modified GLS file.
                    # Note that this changes the GLS file on disk, but it is restored below.
                    doc.CodeGenerate()
                    
                    # Close the file, and discard changes made by pregen scripts
                    doc.Close()
                    Editor.ForceEditorRefresh()
                    
                    # Move the temp file back overtop of this docs file.
                    shutil.move( tempFileName, fName )
                
                else:
                    doc.CodeGenerate() # pregen scripts do not need to run for this file
            
            # else no need to generate, src files are up to date
            
        startingDoc = Document.OpenDocument( startingDocFileName )
        topDoc = Document.OpenDocument( topDocFileName )
        
    def Generate( self ):    
        startingDoc = Document.GetCurrentDocument()
            
        topFileName = ProjectConfiguration.current.topLevelGls
        topDoc = Document.OpenDocument( topFileName )
    
        # Save the active code generator
        startingActiveGenerator = Editor.GetActiveCodeGenerator()
        if startingActiveGenerator != self.codeGeneratorName:
            Editor.SetActiveCodeGenerator( self.codeGeneratorName )
        else:
            startingActiveGenerator = None

        if self.generatorOptions:
            Editor.SetCodeGeneratorOptions( self.generatorOptions )

        # Changing the generation mode dirties the .gls file, even if it doesn't change, so avoid changing it
        startingGenerationMode = topDoc.GetCodeGenerationMode( )
        if startingGenerationMode != self.topLevelMode:
            topDoc.SetCodeGenerationMode( self.topLevelMode )
        else:
            startingGenerationMode = None
            
        if self.pregenScripts: # Pregen and generate
            self.PregenAndGenerate()
            
        else: # just generate
            topDoc.CodeGenerateChildren()
            
            self.CreateOutputListForCurrentGeneratorForDoc(topDoc)

        # Put the generation mode back where we found it
        if startingGenerationMode:
            topDoc.SetCodeGenerationMode( startingGenerationMode )

        # Put the active code generator back where we found it
        if startingActiveGenerator:
            Editor.SetActiveCodeGenerator( startingActiveGenerator )
        
        if startingDoc and not startingDoc.isNULL():
            startingDoc.MakeCurrentDocument()

    def CleanAll( self ):
        print "Performing CleanAll in Generator"
        startingDoc = Document.GetCurrentDocument()
            
        topFileName = ProjectConfiguration.current.topLevelGls
        topDoc = Document.OpenDocument( topFileName )

        # Save the active code generator
        startingActiveGenerator = Editor.GetActiveCodeGenerator()
        if startingActiveGenerator != self.codeGeneratorName:
            Editor.SetActiveCodeGenerator( self.codeGeneratorName )
        else:
            startingActiveGenerator = None

        if self.generatorOptions:
            Editor.SetCodeGeneratorOptions( self.generatorOptions )

        ###########
        # Clean up all content that could have ever been generated from this generator.          
        codePath = Editor.GetCodePathForCurrentGenerator( topDoc )
        codePath = os.path.abspath(codePath)
        
        if os.path.exists( codePath ):
            # WARNING: This is going to delete the whole generated target directory.
            # This is the right thing to do for this example project but may be the
            # wrong thing to do in some other setups.
            try:
                # We are going to remove the files, but leave the directories.
                # If we delete the directories, users have to deal with directory-creation 
                # confirmation popups the next time they build.             
                for root, dirs, files in os.walk(codePath, topdown=False):
                    for name in files:
                        os.remove(os.path.join(root, name))            
            except Exception as e:
                print 'Unable to delete:',codePath,e
        ###########
            
        # Put the active code generator back where we found it
        if startingActiveGenerator:
            Editor.SetActiveCodeGenerator( startingActiveGenerator )
        
        if startingDoc and not startingDoc.isNULL():
            startingDoc.MakeCurrentDocument()
            
        