# This file is licensed under the MIT License.
# 
# Copyright (c) 2015 by The DiSTI Corporation.
# 
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
# 
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
# 
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.

""" Creates a SCons Environment suitable for creating a MS Windows executable """

import os
import glob
import sys
import shutil
import SCons.Script
import SCons.Environment
import SCons.Tool.MSCommon.vs

def FindGLSSupportedVSVersions():
    glsDlls = glob.glob(os.environ['GLSTUDIO']+'/bin/glstudio*_vc*.dll')
    # Get the next characters after the '_vc' before the '.'
    
    versionCharacters = [ os.path.basename(x).partition('_vc')[2].partition('.')[0] for x in glsDlls ]
    # Get just the leading numbers "80md" -> "80"
    def justLeadingNumber( stringWithNumber ):
        return ''.join([ x for x in stringWithNumber if x.isdigit() ])   
    versionCharacters = [ justLeadingNumber(x) for x in versionCharacters ]
    
    # Insert a decimal point 110 -> 11.0
    withDecimal = [ (x[:-1]+'.'+x[-1:]) for x in versionCharacters ]
    
    # Make a minimum set
    minimumSet = list(set(withDecimal))
    return minimumSet
    
def Environment( *args, **kwargs ):

    if not 'MSVC_VERSION' in kwargs:
        # Selecting a MSVC version that is installed on the machine AND is provided by the GL Studio installation
        glsSupportedVersions = FindGLSSupportedVSVersions()
        
        installedVersions = SCons.Tool.MSCommon.vs.query_versions()
        
        # Find the versions that are in common.
        # We want 14.0 to equal 14.0Exp so, only compare the first four characters
        inCommon = [v for v in installedVersions if v[:4] in glsSupportedVersions]        
        
        # Now choose the first one which is the highest # (because the query_versions was already sorted)
        if len(inCommon):
            kwargs['MSVC_VERSION'] = inCommon[0]
        else:
            print "ERROR: Couldn't find an installed Visual Studio that is supported by GL Studio.  Stopping build."
            quit()
    
    if not 'TARGET_ARCH' in kwargs:
        kwargs['TARGET_ARCH'] = 'x86'
        
    env = SCons.Script.Environment( *args, **kwargs )
    
    
    env.Append(CCFLAGS= [
        "/analyze-",
        "/GS",
        "/Zc:wchar_t",
        "/W1",
        "/Z7",
        "/Gm-",
        "/Od",
        "/D", "GLS_IMPORT_LIBRARY",    
        "/D", "WIN32",
        "/D", "_WINDOWS",
        "/WX-",
        "/Zc:forScope",             # Force Conformance in for Loop Scope
        "/GR",                      # Enable Run-Time Type Information
        "/Oy-",                     # Disable Frame-Pointer Omission
        "/MD",                      # Use Multithread DLL version of the runt-time library
        "/EHsc",
        "/nologo",
    ])

    env.Append(LINKFLAGS=[
        "/DEBUG",
        "/SAFESEH",
        "/SUBSYSTEM:CONSOLE",
        "/ERRORREPORT:PROMPT",
        "/NOLOGO",
    ])
    
    env.Append(CPPPATH=[
        os.environ['GLSTUDIO'] + "/include",
        os.environ['GLSTUDIO'] + "/plugins/include",
    ])
    
    env.Append(LIBPATH=[
        os.environ['GLSTUDIO'] + "/plugins/lib",
        os.environ['GLSTUDIO'] + "/lib"
    ])
    env.Append(LIBS=[
        "opengl32.lib",
        "glu32.lib",
        "ws2_32.lib",
        "winmm.lib",
        "comctl32.lib",
        "netapi32.lib",
        "kernel32.lib",
        "user32.lib",
        "gdi32.lib",
        "winspool.lib",
        "comdlg32.lib",
        "advapi32.lib",
        "shell32.lib",
        "ole32.lib",
        "oleaut32.lib",
        "uuid.lib",
        "odbc32.lib",
        "odbccp32.lib",
    ])
    # target must be a file name.  source must be an exe.
    def _CopyExeAndDependentGlsDlls( target, source, env ):
        def _GetGLSDLLsNeededByExe( executablePath ):
            result = []
            dumpbinOutputFileName = os.path.abspath(executablePath+'.dumpbin')
            env.Execute('@dumpbin /dependents "'+executablePath+'" > "'+dumpbinOutputFileName+'"')
            with open(dumpbinOutputFileName, 'r') as f:
                lines = f.readlines()
                f.close()
                # Strip the lines
                lines[:] = [x.strip() for x in lines]
                # Only keep lines ending in .dll
                lines[:] = [ x for x in lines if x[-4:].lower() == ".dll" ]
                # Get the list of all files in the bin directory
                binFiles = os.listdir(os.environ['GLSTUDIO'] + '/bin')
                # Keep only the ones in common
                lines[:] = [ x for x in lines if x in binFiles ]
                result = lines
            return result
            
        dlls = _GetGLSDLLsNeededByExe(os.path.abspath(str(source[0])))
        
        # Support target being a directory
        targetPath = str(target[0])
        sourcePath = str(source[0])
        dllTargetDir = os.path.dirname( os.path.abspath(targetPath) )
        dllSourceDir = os.environ['GLSTUDIO'] + '\\bin'
        if os.path.isdir( targetPath ):
            # We are going to use the source name to determine the target name
            targetPath = os.path.join( dllTargetDir, os.path.basename( sourcePath ) )
        shutil.copy2( sourcePath, dllTargetDir )
        for x in dlls:
            shutil.copy2( os.path.join( dllSourceDir, x ), os.path.join( dllTargetDir, x ) )
    
    # destination must be a directory
    def _InstallWithGlsDLLs( destination, source, *args, **kwargs):
        if 'action' in kwargs:
            print "ERROR: 'action' already defined when calling InstallWithGlsDLLs.  It should be undefined."
        else:
            kwargs['action'] = _CopyExeAndDependentGlsDlls
            # Build the target exe from the source file into the destination directory
            target = os.path.abspath(os.path.join( destination, os.path.basename( str(source[0])) ))
            env.Command( target, source, *args, **kwargs) 
            

    # Allows for clean usage in the SConstruct along side other env.Install() calls.
    env.InstallWithGlsDLLs = _InstallWithGlsDLLs
        
    return env
