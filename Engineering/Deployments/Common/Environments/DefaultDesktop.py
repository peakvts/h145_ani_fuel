# This file is licensed under the MIT License.
# 
# Copyright (c) 2015 by The DiSTI Corporation.
# 
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
# 
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
# 
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.

""" Creates a SCons Environment suitable for creating a Desktop GL Studio build on either Windows or Linux """

import os
import glob
import sys
import shutil
import SCons.Script
import SCons.Environment
import platform
import commands
import json
import DefaultDesktopLinux
import DefaultDesktopWindows
import Common.Constants

def Environment( *args, **kwargs ):

    vars = SCons.Script.Variables()
    vars.AddVariables(
            SCons.Script.BoolVariable('DEBUG', 'Set to 0 to build for release', 1),
            );
    kwargs['variables'] = vars 

    env = None
    if 'Linux' in platform.system():
        env = DefaultDesktopLinux.Environment( *args, **kwargs);
    else:
        env = DefaultDesktopWindows.Environment( *args, **kwargs);
        
        
    def _CreateDefaultProgram( config, generatedDir, additionalSourceFiles, additionalRuntimeFiles = [] ):
    
        exeName = "desktopBuild"

        additionalSourceDir = os.path.abspath( config.additionalSourceDir + '/AdditionalSource')

        sourceFiles = additionalSourceFiles
        resourceFiles = []

        generatedFiles = None
        jsonFileName = generatedDir +"/"+ Common.Constants.fileNameOfGeneratedFileList
        with open( jsonFileName, "r") as f:
            generatedFiles = json.load( f )

        if not generatedFiles:
            print "Error getting generated file list from:", jsonFileName, "aborting build."
            return
        elif 'TOP_LEVEL_CLASS' not in generatedFiles:
            print "TOP_LEVEL_CLASS expected in", jsonFileName, "aborting build."
            return
    
        sourceFiles.extend( generatedFiles['SOURCE'] )
        resourceFiles.extend( generatedFiles['RESOURCE'] )
        # NOTE: We don't have to tell SCons about the header files because it will find the dependencies in the cpp files.
            
        intermediateDir = 'intermediate'
        distDir = 'dist'
        if ('DEBUG' not in env) or env['DEBUG']:
            intermediateDir += '/Debug'
            distDir += '/Debug'
        else:    
            intermediateDir += '/Release'
            distDir += '/Release'
            
        env.Append(CPPPATH = [
            # Identify directories that will be searched for .h files.
            generatedDir,
            config.additionalSourceDir,
            config.alternateClassBehaviorDir,
            ])

        env.Append(CCFLAGS = [
                "-DGLSGEN_"+generatedFiles['TOP_LEVEL_CLASS']+"_STANDALONE",
            ])
        if platform.system() == 'Linux':
            env.Append(CCFLAGS = [
                # Add any flags here for the compilation
                "-std=c++0x",
            ])
        else:
            env.Append(CCFLAGS = [
                # Add any flags here for the compilation
                "-DNOMINMAX", # To avoid conflicts with Windows headers
            ])

        env.Append(LIBPATH = [
            # Add any paths to libraries here
        ])

        env.Append(LIBS = [
            # Add any additional libraries needed by the code here
        ])


        # Using VariantDir allows us to keep the temporary files out of the generated folder.
        # Instead they go to the 'intermediate' directory.
        generatedVariantDir = os.path.abspath(intermediateDir+'/gls_generated')
        generatedDirAbs = os.path.abspath( generatedDir )
        env.VariantDir(generatedVariantDir, generatedDirAbs, duplicate=0)
        sourceFilesInVariantDirs = [os.path.abspath(w).replace(generatedDirAbs, generatedVariantDir) for w in sourceFiles]

        additionalSourceDir = os.path.abspath(config.additionalSourceDir) # abspath to be consistent with ones being replaced
        additionalSourceVariantDir = os.path.abspath(intermediateDir+'/additional_source')
        env.VariantDir(additionalSourceVariantDir, additionalSourceDir, duplicate=0)
        sourceFilesInVariantDirs = [os.path.abspath(w).replace( additionalSourceDir, additionalSourceVariantDir) for w in sourceFilesInVariantDirs]
        
        program = env.Program(
            source=sourceFilesInVariantDirs, 
            target=intermediateDir+'/program_build/'+exeName)

        for a in additionalRuntimeFiles:
            env.Install(distDir, source = a)     
        env.Install(distDir+'/resources', source = glob.glob(generatedDirAbs + '/resources/*'))
        env.InstallWithGlsDLLs(distDir, program)

        ##############################################
        ## Creates a Visual Studio project for viewing the related source code.
        ## If you don't need a VS project, you can safely remove this section since 
        ## it is not required to perform the build.
        ##############################################
        if 'Linux' not in platform.system(): # You probably don't need a Visual Studio project when building on Linux
            buildSettings = {
                'LocalDebuggerWorkingDirectory':'..\\'+distDir
            }

            if True:
                # If you want to ensure that all generated code is up to date when you build use this:
                env['MSVSSCONSCOM'] = '..\\build.bat'  # This generates, and then calls scons.bat
            else:
                # If you want to do quick builds while working on non-generated source, use this:
                env['MSVSSCONSCOM'] = '..\\scons.bat'   

            possibleIncludes = \
                glob.glob( config.alternateClassBehaviorDir + '/*.h' ) +\
                glob.glob( config.additionalSourceDir + '/*.h' )

            env.MSVSProject(target = 'visualc/DesktopBuild' + env['MSVSPROJECTSUFFIX'],
                            srcs = sourceFiles,
                            incs = possibleIncludes + generatedFiles['HEADER'],
                            buildtarget = program,
                            variant = 'Debug',
                            DebugSettings = buildSettings)

    env.CreateDefaultProgram = _CreateDefaultProgram
    
    return env
