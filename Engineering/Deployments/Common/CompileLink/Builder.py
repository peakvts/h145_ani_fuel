# This file is licensed under the MIT License.
# 
# Copyright (c) 2015 by The DiSTI Corporation.
# 
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
# 
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
# 
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.

""" Provides a Builder object which is designed to execute a SCons command line build """

import Editor
import Document
import os
import ComponentRef
import json
import threading
import subprocess
import ProjectConfiguration
import Common.EditorThreadQueueUtil as editor
import signal
import platform
import shutil

# We only want one build process to run at the same time, so here we have
# storage to keep track, long-term, of a previous build processes
existingBuildProcess = None

class Builder:
    def __init__( self, buildDirectory, buildCommand, buildArguments, commandLine ):
        self.buildDirectory = buildDirectory
        self.buildCommand = buildCommand
        self.buildArguments = buildArguments
        self.debugBuild = True
        
        self.doingCommandLineBuild = False
        cmdLineSplit = commandLine.split() 
        # Expecting the command line to end in DEBUG or RELEASE
        lastArg = cmdLineSplit[-1] # Get the last argument
        if "DEBUG"== lastArg or "RELEASE"==lastArg:
            # We must be running from the command line
            self.doingCommandLineBuild = True
            if "RELEASE" == lastArg:
                self.debugBuild = False
                # Assuming we are doing a SCons build.  Change this as necessary.
                self.buildArguments.append("DEBUG=0")
        
    def BackgroundBuild( self, callOnSuccess = None ):
        def worker():
            success = self.Build()
            if success and callOnSuccess:
                editor.CallOnMainThread( callOnSuccess )
            
        t = threading.Thread(target=worker)
        t.daemon = not self.doingCommandLineBuild
        t.start()
        
    def Build( self ):
        """ Performs the SCons build.
            Intended to be called in a worker thread.  
            directory is the folder where the scons file should be found.
            sconsArguments is a list of additional arguments to add to the scons command line
        """
        global existingBuildProcess

        # Terminate another build if there is one
        self.TerminateExistingBuildProcess()
        
        command = [os.path.abspath( os.path.join( self.buildDirectory, self.buildCommand ) )]
        if self.buildArguments:
            command.extend( self.buildArguments )
        if platform.system() == 'Linux':
            proc = subprocess.Popen(command, cwd=self.buildDirectory, stderr=subprocess.STDOUT, stdout=subprocess.PIPE)
        else:
            proc = subprocess.Popen(command, cwd=self.buildDirectory, stderr=subprocess.STDOUT, stdout=subprocess.PIPE, creationflags=subprocess.CREATE_NEW_PROCESS_GROUP)
        
        existingBuildProcess = proc

        success = self.WatchBuildProcessUntilEnd( proc )
            
        existingBuildProcess = None
        
        return success
    
    # Returns True if build is considered a success
    def WatchBuildProcessUntilEnd(self, proc):
        global existingBuildProcess
        # Watch the build process    
        while None==proc.poll():
            one_line_output = proc.stdout.readline().rstrip()
            one_line_output_lower = one_line_output.lower();
            try:
                if "error:" in one_line_output_lower or "error :" in one_line_output_lower or ": error" in one_line_output_lower:
                    editor.LogError( one_line_output )
                elif "..." in one_line_output_lower:
                    editor.LogInfo( one_line_output )
                elif ProjectConfiguration.current.verboseBuildOutput:
                    # Only logging every line if verbose has been selected
                    editor.LogInfo( one_line_output )
            except:
                print "Exception logging output in",__file__
                pass # Blocking the exception handling because throwing an exception here can deadlock the editor
                
            print one_line_output # Send it to the console to help debugging
            
        # Check the result of the build        
        rval = proc.returncode;    
        success = False
        if existingBuildProcess:
            if rval != 0:
                editor.LogError("Build failed; see log for details.")
            else:
                editor.LogInfo("Build succeeded.")
                success = True
        else:
            pass # die quietly
    
        return success
    
    def TerminateExistingBuildProcess(self):
        global existingBuildProcess
        if existingBuildProcess:
            if None == existingBuildProcess.returncode:
                # Process is still running, we are going to terminate it
                editor.LogInfo("Stopping previous process.")
                existingBuildProcess.send_signal( signal.CTRL_C_EVENT )
                #existingBuildProcess.terminate()
                editor.LogInfo("Waiting for process to end.")
                existingBuildProcess.wait()
                editor.LogInfo("Process ended")
                
            existingBuildProcess = None

    
    def CleanAll(self):
        print "Performing CleanAll in Builder"
        # This is expected to either be kept in sync with the project build Environments
        # or changed to use the build system (like SCons) to do the clean.
        deleteTheseDirectories = [ 
            os.path.abspath( self.buildDirectory + '/dist' ),
            os.path.abspath( self.buildDirectory + '/intermediate' ),
            os.path.abspath( self.buildDirectory + '/visualc' ),
            ]
        for d in deleteTheseDirectories:
            if os.path.exists( d ):
                try:
                    shutil.rmtree( d ) 
                except Exception as e:
                    print 'Unable to delete:',d,e
                    
                    