# This file is licensed under the MIT License.
# 
# Copyright (c) 2016 by The DiSTI Corporation.
# 
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
# 
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
# 
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.

""" Builds the Deployment based on the provided command line arguments """

import Document
import EditorLog
import os, sys
import Editor
import time
import threading
import subprocess
import webbrowser
import PythonScriptEngineCmdLine

configurationDir = os.path.abspath("../../..")
if not configurationDir in sys.path:
    sys.path.append(configurationDir)    
import ProjectConfiguration

deploymentsDir = os.path.abspath("..")
if not deploymentsDir in sys.path:
    sys.path.append(deploymentsDir)    

from Common.Generation import Generator
from Common.CompileLink import Builder
import Common.Generation.Collectors.ES
import Common.Generation.Extra.ApplicationDefaults
from Common.Run import Runner
import Common.Run.Runners.Html as runnerModule
import Common.Generation.Extra.ApplicationDefaults

commandLine = ""
currentLocals = locals()
if 'execFileCommandline' in currentLocals:    
    # Grab the arguments from the variables we provided when calling execfile.  
    commandLine = currentLocals['execFileCommandline']
else:
    # Use the command line given to GL Studio
    commandLine = PythonScriptEngineCmdLine.GetCmdLine()
commandLineSplit = commandLine.split()
    
runOnSuccess = "RUN_ON_SUCCESS" in commandLineSplit
cleanAll = "CLEAN_ALL" in commandLineSplit    

# Import scripts for pregeneration before including below
import Common.Generation.PregenScripts.ConvertSpheresAndCylindersToMeshes

generator = Generator.Generator(
    Common.Generation.Collectors.ES, 
    ProjectConfiguration.current.topLevelGls, 
    Document.Document.CODE_GENERATION_MODE_COMPONENT, 
    "PVR=false", 
    [Common.Generation.Extra.ApplicationDefaults], 
    pregenScripts = [
        # List scripts for pregeneration here, after importing above
        # This can be used to convert object types (as is done in the provided example script )
        # or to fetch the latest version of an asset, or to perform automated optimization.
        Common.Generation.PregenScripts.ConvertSpheresAndCylindersToMeshes, # Example Pre-Generate script, replaces Sphere and Cylinder with trimesh (fixes compatibility on ES)
    ] )
builder = Builder.Builder( ProjectConfiguration.current.webGLBuildDir, "./scons.bat", ["--debug=explain"], commandLine)
runner = Runner.Runner( runnerModule, ProjectConfiguration.current.webPortal.webGLLaunchPath, None )

if not cleanAll:
    EditorLog.PrintInfo("Generating ...")
    generator.Generate( )

    def run():
        global runner
        runner.Run()
        
    EditorLog.PrintInfo("Compiling ...")
    if runOnSuccess:
        builder.BackgroundBuild( run )
    else:
        builder.BackgroundBuild(  )
else:
    EditorLog.PrintInfo("Cleaning...")
    generator.CleanAll()
    builder.CleanAll()    
    EditorLog.PrintInfo("Clean succeeded.")
