//----------------------------------------------------------------------------
//
// This files is *NOT GENERATED*.  It is only created once from the wizard.
// Please manually modify this as necessary.
//
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
// This file is licensed under the MIT License.
// 
// Copyright (c) 2015 by The DiSTI Corporation.
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
//-----------------------------------------------------------------------------

#include <stdlib.h>
#include <esUtil.h>
#include <emscripten.h>
#include <emscripten/html5.h>

#include <emscripten_display_frame.h>
#include <image_loaders.h>
#include <statistics.h>
#include <gls_resources.h>

#include "application_defaults.h"

using namespace disti;

////////////////////////////////////
#define TOP_LEVEL_COMPONENT_CLASS APPLICATION_DEFAULT_CLASS_NAME
//#define SHOW_RESOURCE_CONTROL_BUTTON
////////////////////////////////////

TOP_LEVEL_COMPONENT_CLASS *topComponent = NULL;

ESContext *esContext = NULL;
EmscriptenDisplayFrame *edf = NULL;

int loadingCount = 0;
void onLoad()
{
    std::cout << "Loaded\n";

    loadingCount--;
}
void errorOnLoad()
{
    std::cout << "Error Loading\n";

    loadingCount--;
}

class CallbackContainer
{
public:
    int HandleCallback(DisplayObject* self, DisplayEvent* ev)
    {
        if (ObjectEvent* oev = dynamic_cast<ObjectEvent*>(ev))
        {
            // First check to see if we have a callback
            int hasCallback = EM_ASM_INT_V({
                if (Module.disti && Module.disti.onobjectevent)
                    return 1;
                else
                    return 0
            });

            if (hasCallback)
            {
                std::string qualifiedName;
                if (oev->Initiator())
                    qualifiedName = GetQualifiedInstanceName(topComponent, oev->Initiator());

                std::ostringstream str;
                str <<"Module.disti.onobjectevent('"<<qualifiedName<<"', '"<<oev->EventName()<<"', '"<<oev->EventData()<<"');";

                // Do the callback
                emscripten_run_script(str.str().c_str());
            }
        }

        return 0;
    }
};

CallbackContainer callbackContainer;


void Draw()
{
    static int frame = 0;

    switch (frame)
    {
    case 0: {
        // Wait until other dependencies are loaded, otherwise, emscripten_async_load_script will fail
        if (emscripten_run_script_int("runDependencies")>0)
            break;

        // If the resources are in a different file, load it here
        loadingCount++;
        emscripten_async_load_script("resources.js", onLoad, errorOnLoad);

        emscripten_run_script("Module.setStatus('Loading Resources...');");

        if ( APPLICATION_DEFAULT_FULL_SCREEN )
        {
            emscripten_run_script("Module.setFullscreenAvailable(true);");
        }

        #ifdef SHOW_RESOURCE_CONTROL_BUTTON
            emscripten_run_script("Module.setResourceControlsAvailable(true);");
        #endif


        frame++;
        break;
    }
    case 1: {
        // Keep waiting if we are still loading resources
        if (loadingCount > 0)
            break;

        emscripten_run_script("Module.setStatus('Constructing Model...');");
        // The model must be constructed in the Draw()
        topComponent = new TOP_LEVEL_COMPONENT_CLASS("topLevelComponent");


        frame++;
        break;
    }
    case 2: {
        emscripten_run_script("Module.setStatus('Creating Objects...');");

        topComponent->AbsolutePlacement(true);
        topComponent->CreateObjects();

        edf->DisplayFrame::InsertObject(topComponent,0);

        edf->objects->CallbackCaller(new CallbackMethodCallerTemplate< CallbackContainer, DisplayObject >(
            (disti::CallbackMethodCallerTemplate< CallbackContainer, DisplayObject >::MethodType1)
             &CallbackContainer::HandleCallback, &callbackContainer ));

        #if APPLICATION_DEFAULT_PROJECTION_MODE == 0 //ORTHOGRAPHIC
            // The default
        #elif APPLICATION_DEFAULT_PROJECTION_MODE == 1 //PERSPECTIVE
            edf->CurrentView(VIEW_PERSPECTIVE);
        #elif APPLICATION_DEFAULT_PROJECTION_MODE == 2 //EYEPOINT
            edf->CurrentView(VIEW_EYEPOINT);
            edf->CurrentEyePoint(topComponent->APPLICATION_DEFAULT_INITIAL_EYE_POINT_NAME);
        #endif

        emscripten_run_script("Module.setStatus('Running');");
        frame++;
        break;
    }
    case 3: {
        // Wait for disti texture loading to be complete
        if (emscripten_run_script_int("Module.disti.texture_loading") != 0)
        {
            // Keep waiting
            emscripten_run_script("Module.setStatus('Loading Textures...');");
        }
        else
        {
            emscripten_run_script("Module.setStatus('Running');");
            emscripten_run_script("Module.setSpinner(false);");

            frame++;
        }
        break;
    }
    default: {
        // Fully running
    }
    }

    static clock_t timeZero = clock();
    static clock_t lastClockTime = timeZero;
    static clock_t pauseOffset = 0;
    clock_t newClockTime = clock();
    if (emscripten_run_script_int("Module.disti.texture_loading") != 0)
    {
        pauseOffset += newClockTime - lastClockTime;
    }

    lastClockTime = newClockTime;

    if (topComponent)
    {
        double timeSinceStartWithPause = 1.0 * (newClockTime - (timeZero+pauseOffset)) / CLOCKS_PER_SEC;
        double timeSinceStart = 1.0 * (newClockTime - (timeZero)) / CLOCKS_PER_SEC;

        edf->objects->Calculate( timeSinceStartWithPause );

        edf->Redraw();
    }
}


extern "C" const char* EMSCRIPTEN_KEEPALIVE GetResourceToFree(const char* resourceName)
{
    if (topComponent)
    {
		std::string value = topComponent->GetResource(resourceName);

		char* returnVal = new char [value.size()+1];
		strcpy(returnVal, value.c_str());

		return returnVal;
    }
    else
    {
		char* returnVal = new char [1];
		returnVal[0] = '\0';
		return returnVal;
    }
}
extern "C" void EMSCRIPTEN_KEEPALIVE GetResourceFreeMemory(const char* toFree)
{
	delete [] toFree;
}

extern "C" void EMSCRIPTEN_KEEPALIVE SetResource(const char* resourceName, const char* value)
{
    if (topComponent)
    {
        topComponent->SetResource(resourceName, value);
    }
}
extern "C" void EMSCRIPTEN_KEEPALIVE ToggleStatistics()
{
    if (edf)
    {
        edf->Stats()->ToggleDisplay();
    }
}

void possiblyLockToServer()
{
	#ifdef DO_LOCK_TO_SERVER
    #define LOCK_TO_SERVER "disti.com"
	#endif
    #ifdef LOCK_TO_SERVER
        const char* hostname = emscripten_run_script_string("window.location.hostname");
        if (std::string(hostname).find( LOCK_TO_SERVER ) == -1)
        {
            // Not hosted on the right server so we are not going to run
            emscripten_run_script((std::string("document.body.style.backgroundColor = 'white';\ndocument.body.innerHTML = 'This content must be served from: ") + LOCK_TO_SERVER + "'").c_str());
            exit(-1);
        }
    #endif
}

void fitToLandscape(int& width, int& height)
{
    int largestDimension = EM_ASM_INT_V({

        var viewport = document.querySelector('meta[name=viewport]');
        viewport.setAttribute('content', 'width=device-height, height=device-width, user-scalable=no, minimum-scale=1.0, maximum-scale=1.0');

        function MIN(A,B) { if (A > B) return B; else return A; };
        function MAX(A,B) { if (A < B) return B; else return A; };

        var clientWidth = MIN(document.documentElement.clientWidth, window.innerWidth);
        var clientHeight = document.documentElement.clientHeight;

        var largestDimension = MAX(clientWidth, clientHeight);

        return largestDimension;
    });

    int newWidth = MIN(width,largestDimension);
    //int newWidth = largestDimension;

    int newHeight = (newWidth) * height / width;
    width = newWidth;
    height = newHeight;
}

int main ( int argc, char *argv[] )
{
    possiblyLockToServer();

    loadImageLoaders();

    // Exposes some calls to the containing JavaScript page.
    EM_ASM({
        // Make sure we have the disti module for some data passing
        if (Module && !Module.disti) {
            Module.disti = new Object();
        }

        SetResource = Module.cwrap('SetResource', null,    ['string', 'string']);

        GetResourceToFree = Module.cwrap('GetResourceToFree','number', ['string']);
        GetResourceFreeMemory = Module.cwrap('GetResourceFreeMemory',null, ['number']);
        GetResource = function(name) {
			value = GetResourceToFree(name);
			newString = Pointer_stringify(value);
			GetResourceFreeMemory(value);
			return newString;
        };

        ToggleStatistics = Module.cwrap('ToggleStatistics', null, null);

    });

    esContext = new ESContext();
    esInitContext ( esContext );

    int width = APPLICATION_DEFAULT_WIDTH;
    int height = APPLICATION_DEFAULT_HEIGHT;
    //Activate as desired: 
	//fitToLandscape(width, height);

    esCreateWindow ( esContext, APPLICATION_DEFAULT_WINDOW_TITLE, width, height, ES_WINDOW_RGB | ES_WINDOW_MULTISAMPLE );

    edf = new EmscriptenDisplayFrame(APPLICATION_DEFAULT_WINDOW_TITLE, esContext->width, esContext->height, esContext);

    edf->BackgroundColor( APPLICATION_DEFAULT_BACKGROUND_COLOR );

    edf->FrameInterval(33333); // Just used for statistics

    emscripten_set_main_loop(Draw, 0, true); // 0 means run as fast as the browser will allow
}
