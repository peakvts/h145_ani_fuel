:: This file is licensed under the MIT License.
:: 
:: Copyright (c) 2015 by The DiSTI Corporation.
:: 
:: Permission is hereby granted, free of charge, to any person obtaining a copy
:: of this software and associated documentation files (the "Software"), to deal
:: in the Software without restriction, including without limitation the rights
:: to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
:: copies of the Software, and to permit persons to whom the Software is
:: furnished to do so, subject to the following conditions:
:: 
:: The above copyright notice and this permission notice shall be included in
:: all copies or substantial portions of the Software.
:: 
:: THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
:: IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
:: FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
:: AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
:: LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
:: OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
:: THE SOFTWARE.

@echo off
setlocal

:: First we are going to configure the environment for an EMSCRIPTEN build
set EMSCRIPTEN_PATH=%GLSTUDIO%\GLStudioWebGLRuntime\emscripten
if not exist "%EMSCRIPTEN_PATH%" (
	echo "GL Studio WebGL does not appear to be installed in %GLSTUDIO%"
	exit /b -1
)

:: Make sure not to use the system python.  We just want the one in emscripten for the build.
set PYTHONHOME=
set ORIG_PATH=%PATH%
set PATH=C:\Windows\System32

call "%EMSCRIPTEN_PATH%\emsdk.bat" activate >NUL

:: Test to make sure we did a successful emsdk activation
if "%PATH%"=="C:\Windows\System32" (
    echo Error activating from "%EMSCRIPTEN_PATH%\emsdk.bat"
    exit /b -1
)

:: Include our original path, at the end of the new path.
set PATH=%PATH%;%ORIG_PATH%

set EMSCRIPTEN_BROWSER=%EMSCRIPTEN%\emrun.bat

:: Now actually call SCons with all of the original arguments
"%GLSTUDIO%\bin\python_in_glstudio_bin.exe" "%GLSTUDIO%\bin\scons-local\scons.py" %*
:: Return the results of the SCons call
exit /b %ERRORLEVEL%
