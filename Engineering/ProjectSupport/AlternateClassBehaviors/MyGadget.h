/** \file
  * \brief Provides a simple alternate class that can be used as a starting point for creating other alternate classes.
  */

//-----------------------------------------------------------------------------
// This file is licensed under the MIT License.
// 
// Copyright (c) 2015 by The DiSTI Corporation.
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
//-----------------------------------------------------------------------------

#ifndef INCLUDED_MY_GADGET_H
#define INCLUDED_MY_GADGET_H

/* $ALTERNATE_CLASS_NAME_BEGIN$
<?xml version="1.0" encoding="UTF-8"?>
<AlternateClassName name="MyGadget<%ORIGINAL_CLASS%>">

    <!-- Add properties to be displayed in the Object Properties tab here. Sample properties: -->
	<Property internalName="SpinSpeed"     displayName="Spin Speed"     groupName="Gadgetry" description="Sets the speed at which the object will rotate." proptype="float32"        defaultValue="0.0"                  />
</AlternateClassName>    
$ALTERNATE_CLASS_NAME_END$*/

#include "events.h"
#include "gls_metadata_attributes.h"
#include "gls_mutex_group.h"
#include "project_util.h" // For helper macros

namespace disti
{

template <class T>
class MyGadget : public T
{
public:
    ///////////////////////////////////////////////////////////////////////////////////////////////
    // Types
    ///////////////////////////////////////////////////////////////////////////////////////////////
    typedef T        BaseClass;
    typedef MyGadget ThisClass;

    ///////////////////////////////////////////////////////////////////////////////////////////////
    /// Constructor
    ///////////////////////////////////////////////////////////////////////////////////////////////
    explicit MyGadget( bool generateInstance = false )
        : BaseClass( generateInstance )
        , _firstCalculate( true )
		, _spinSpeed( 0 )
    {
        DISTI_STATIC_ASSERT_IS_CONVERTIBLE_TO( BaseClass, Group ); // The base class must be related to Group by inheritance; could use Group or similar

        // Setup event handler callback
        DISTI_REGISTER_METHOD_CALLBACK( this, ThisClass, &ThisClass::EventCallback );
    }
    
    ///////////////////////////////////////////////////////////////////////////////////////////////
    /// This function gets called regularly to update the object with the current time
    ///////////////////////////////////////////////////////////////////////////////////////////////
    void Calculate( const double time ) DISTI_METHOD_OVERRIDE
    {
        // On the first call to Calculate(), we need to register our properties with anyone listening.
        // This can't be done in the constructor, because not everything is ready to listen at that point.
        if( _firstCalculate )
        {
            _firstCalculate = false;

            // Sample code to register properties with any object listening for this event for 
            // discovering properties at runtime. In the case of the UI Designer workflow sample
            // project, the listener is the Javascript embedded in shell.html, which uses 
            // this to build the property table for testing and preview in WebGL, but other listeners
            // can also be added.
            
            // Each registration should correspond to an attribute coming either from the base class 
            // or added in SetAvailableAttributes().            
            EmitObjectEvent( this, "REGISTER_PROPERTY", "SpinSpeed FLOAT"            );
        }
		
		BaseClass::DynamicRotate( _spinSpeed * time, Z_AXIS );

        BaseClass::Calculate( time );
    }

    ///////////////////////////////////////////////////////////////////////////////////////////////
    // Setup additional attributes provided by this class. See more about attributes in the User Manual.
    ///////////////////////////////////////////////////////////////////////////////////////////////
    void SetAvailableAttributes( const unsigned int availableAttributes ) DISTI_METHOD_OVERRIDE
    {
        BaseClass::SetAvailableAttributes( availableAttributes );

        // ... add any additional attributes added to this class here (compare the "first calculate" comments above).
        // For example:
        DISTI_ADD_ATTRIBUTE( float,   "SpinSpeed",    &_spinSpeed );
    }

    ///////////////////////////////////////////////////////////////////////////////////////////////
    // Override the existing handle method to prevent highlighting on local updates.
    ///////////////////////////////////////////////////////////////////////////////////////////////
    DisplayObject* handle( DisplayEvent* const ev ) DISTI_METHOD_OVERRIDE
    {
        return BaseClass::handle( ev );
    }
    
private:
    ///////////////////////////////////////////////////////////////////////////////////////////////
    // Handles GL Studio events for this object.
    ///////////////////////////////////////////////////////////////////////////////////////////////
    int EventCallback( Group* const self, DisplayEvent* const ev )
    {
        const MouseEvent*    const mev = (ev->eventType == EVENT_MOUSE    ? static_cast<MouseEvent*>(ev)    : NULL);
        const KeyboardEvent* const kev = (ev->eventType == EVENT_KEYBOARD ? static_cast<KeyboardEvent*>(ev) : NULL);
        const ObjectEvent*   const oev = dynamic_cast< ObjectEvent* >( ev );

        // ... handle events here ... return true if handled, false if not.
        // ... emit new events as properties change or as otherwise necessary ...

        return false;
    }

    ///////////////////////////////////////////////////////////////////////////////////////////////
    // Data members
    ///////////////////////////////////////////////////////////////////////////////////////////////
    bool _firstCalculate;

    // Sample attribute member variables
    float _spinSpeed;
};

}
        
#endif