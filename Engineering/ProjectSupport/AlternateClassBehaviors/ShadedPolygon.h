/** \file
  * \brief Provides an alternate class to apply a shader to a GlTriMesh
  */

//-----------------------------------------------------------------------------
// This file is licensed under the MIT License.
// 
// Copyright (c) 2015 by The DiSTI Corporation.
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
//-----------------------------------------------------------------------------

#ifndef INCLUDED_SHADED_POLYGON_H
#define INCLUDED_SHADED_POLYGON_H

#ifndef GLES
#  error "This class requires GL Studio ES."
#endif

/* $ALTERNATE_CLASS_NAME_BEGIN$
<?xml version="1.0" encoding="UTF-8"?>
<AlternateClassName name="ShadedTriMesh">
    <Property internalName="vertShader" displayName="Vertex Shader"   groupName="Shaders" description="Vertex Shader. Compiled at run time."   proptype="utf8_multiline" defaultValue=""/>
    <Property internalName="fragShader" displayName="Fragment Shader" groupName="Shaders" description="Fragment Shader. Compiled at run time." proptype="utf8_multiline" defaultValue=""/>
</AlternateClassName>
$ALTERNATE_CLASS_NAME_END$*/

#include "gltrimesh.h"
#include "gls_state_manager.h"
#include "vertex.h"
#include "gls_metadata_attributes.h"

#include "project_util.h" // For helper macros
#include "gls_es20_effect_factory.h"

namespace disti
{
    class ShadedTriMesh: public GLTriMesh 
    {
        typedef GLTriMesh BaseClass;

    public:

        // Modifiers
        void Update( float time )           { _time     = time; }
        void Modifier( float modifier )     { _modifier = modifier; }
        void Offset( float offset )         { _offset   = offset; }
        void Palette( TexturePalette* pal ) { _palette  = pal; }
        void Color( glsColor col )          { _mixColor = col; }
        
        void Calculate( const double time ) DISTI_METHOD_OVERRIDE
        {
            static bool firstCalc = true;

            if( firstCalc )
            {
                firstCalc = false;
                EmitObjectEvent( this, "REGISTER_PROPERTY", "Color STRING" );
                
                Init();
            }
        }

        void SetAvailableAttributes( const unsigned int availableAttributes ) DISTI_METHOD_OVERRIDE
        {
            BaseClass::SetAvailableAttributes( availableAttributes );

            // Add any additional attributes added to this class here
            DISTI_ADD_ATTRIBUTE( glsColor, "Color", &_mixColor );
            DISTI_ADD_ATTRIBUTE_STRING( "fragShader", &_fragmentShader );
            DISTI_ADD_ATTRIBUTE_STRING( "vertShader", &_vertexShader );
        }

        /** Constructor */
        ShadedTriMesh()
            : _mixColor( 255, 0, 255, 255 )
            , _time( 0.0f )
            , _modifier( 0.0f )
            , _offset( 0.0f )
            , _uTex( UINT_MAX )
            , _uGrad( UINT_MAX )
            , _uUpModOff( UINT_MAX )
            , _uColor( UINT_MAX )
            , _fragmentShader()
            , _vertexShader()
        {
        }

        // Overloaded Draw method
        void Draw() DISTI_METHOD_OVERRIDE
        {

            IGlsStateManagerES20 *stateManager = dynamic_cast< IGlsStateManagerES20* >( GlsStateManager::Instance() );

            stateManager->PushCustomShaderProgram( _shaderEffect, true );

                glActiveTexture( GL_TEXTURE0 );
                _palette->BindTexture( 1, GlsStateManager::Instance() );
                glActiveTexture( GL_TEXTURE1 );
                _palette->BindTexture( 2, GlsStateManager::Instance() );
                glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR );
                glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR );
                glActiveTexture( GL_TEXTURE0 );

                float vals[3] = { _time, _modifier, _offset };
                _shaderEffect->GetCustomUniform( _uUpModOff )->SetUniform3f( vals );
                _shaderEffect->GetCustomUniform( _uTex )->SetUniform1i( 0 );
                _shaderEffect->GetCustomUniform( _uGrad )->SetUniform1i( 1 );   

                float color[4];
                _mixColor.GetRGBA4f( color[0], color[1], color[2], color[3] );
                _shaderEffect->GetCustomUniform( _uColor )->SetUniform3f( color );

                BaseClass::Draw();

            stateManager->PopCustomShaderProgram();
        }
    private:
        
        float           _time;
        float           _modifier;
        float           _offset;

        TexturePalette* _palette;
        glsColor        _mixColor;

        // Custom Shader Uniforms
        GLuint          _uTex;
        GLuint          _uGrad;
        GLuint          _uUpModOff;
        GLuint          _uColor;

        GlsEffect*      _shaderEffect;

        std::string     _vertexShader;
        std::string     _fragmentShader;
        

        void Init()
        {
            // Create the shaders, compile them, and grab uniforms
            _shaderEffect = GlsEffectFactory::Instance()->CreateCustomEffect( _vertexShader.c_str(), _fragmentShader.c_str() );
            // Get uniform handles
            _uTex         = _shaderEffect->CreateCustomUniform( "inputImageTexture" );
            _uGrad        = _shaderEffect->CreateCustomUniform( "gradTex" );
            _uUpModOff    = _shaderEffect->CreateCustomUniform( "upModOff" );
            _uColor       = _shaderEffect->CreateCustomUniform( "uColor" );
        }
    };
}
#endif