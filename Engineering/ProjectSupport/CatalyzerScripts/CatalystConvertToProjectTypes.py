# This file is licensed under the MIT License.
# 
# Copyright (c) 2015 by The DiSTI Corporation.
# 
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
# 
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
# 
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.

"""
For all children of a Catalyzer that are of one class (here, GlsTextGrid), 
set the Alternate Class Name to a project-specific value (here, "MyTextGrid"). 
Most useful in a pipeline of Catalyzer scripts.
"""

from Group import Group
from GlsTextGrid import GlsTextGrid


def SetAlternateClassName( group ):
    for i in range(group.Count()):  # Iterate over all the objects in the group
        child = group.GetObjectByIndex(i)
        textGrid = GlsTextGrid.CastToGlsTextGrid( child )  # Check if this is a GlsTextGrid
        if not textGrid.isNULL():
            child.AlternateClassName( "MyTextGrid" )  # If so, set its AlternateClassName property
        
        # Recurse into groups to look for more text grids
        childGroup = Group.CastToGroup( child )
        if not childGroup.isNULL():
            SetAlternateClassName( childGroup )


def Catalyzer_DoReaction( catalyzerInstance ):
    SetAlternateClassName( catalyzerInstance )
