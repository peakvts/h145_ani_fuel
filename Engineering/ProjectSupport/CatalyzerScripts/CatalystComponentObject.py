# This file is licensed under the MIT License.
# 
# Copyright (c) 2015 by The DiSTI Corporation.
# 
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
# 
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
# 
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.

""" Catalyst - Duplicates the content of the specified GLS file into this catalyzer and refreshes when the source file changes. """

import Document
import os
from catalystUtil import CatalyzerSourceChangeMonitor
from Group import Group
from Catalyzer import Catalyzer
import Queue


# Breadth First Search
def RefreshCatalyzers( obj ):
    searchQueue = Queue.Queue();
    searchQueue.put( obj )
    
    while not searchQueue.empty():
        object = searchQueue.get()
        catalyzer = Catalyzer.CastToCatalyzer(object)
        if( not catalyzer.isNULL() ):
            print( "refreshing " + object.GetName() )
            catalyzer.RunScripts()
        group = Group.CastToGroup(object)
        if( not group.isNULL() ):
            for i in range(group.Count()):
                searchQueue.put( group.GetObjectByIndex(i) )

def Catalyzer_GetPropertiesList():
    # [ ( internalName, propType, displayName, groupName, description, defaultValue ), ]
    return [
    ( "ReferencedObjects", "ascii", "Referenced Objects", __name__, "Names the objects to copy into the catalyzer seperated by spaces (if blank, all objects will be copied)", "" ), 
    ( "ReferencedComponent", "utf8_filepath", "Referenced Component", __name__, "The component that contains the referenced objects", "" ), 
    ( "_ReferencedComponent_LastUpdateSignature", "ascii", "", __name__, "", "" ),     
    ]

def Catalyzer_OnScriptAssigned( catalyzerInstance ):
    CatalyzerSourceChangeMonitor.Start( catalyzerInstance, "ReferencedComponent" )

def Catalyzer_OnScriptUnassigned( catalyzerInstance ):
    CatalyzerSourceChangeMonitor.Stop( catalyzerInstance, "ReferencedComponent" )
    
def Catalyzer_OnPropertyChanged( catalyzerInstance, propertyName ):
    if "ReferencedComponent" == propertyName:
        CatalyzerSourceChangeMonitor.Start( catalyzerInstance, "ReferencedComponent" )    
        
def Catalyzer_DoReaction( catalyzerInstance ):

    # open the specified document, copy the specified objects and paste them into the catalyzer's document, then move those objects 
    # into the catalyzer

    startingDocument = Document.GetCurrentDocument()
    originalSelection = startingDocument.GetSelectedObjects()
    
    referencedComponentName = catalyzerInstance.GetAttributeValueString( "ReferencedComponent" );

    if not referencedComponentName:
        return False

    currDoc = Document.OpenDocument(os.path.abspath(referencedComponentName))
    if not currDoc.isNULL() and not (currDoc.GetFilename() == startingDocument.GetFilename()):
    
        currDoc.MakeCurrentDocument()
            
        currDoc.ClearSelections() # clear selections, so that we get a new selection with a top-down order. Otherwise the objects may already be selected but in the wrong order.
            
        objectNames = catalyzerInstance.GetAttributeValueString( "ReferencedObjects" );
        if not objectNames :
            currDoc.SelectAll()
        else :
            currDoc.SelectObjects( objectNames.split() )            
        
        currDoc.CopySelectedObjects()
        currDoc.ClearSelections() # TODO Remove
        
        startingDocument.MakeCurrentDocument()    
        
        startingDocument.ClearSelections(); # need to clear seletions just in case there are no objects in the below paste, in which case our catalyzer would still be selected
        startingDocument.PasteClipboardObjects( True, True );
        
        newSelectedObjects = startingDocument.GetSelectedObjects()
        
        for i in range(newSelectedObjects.GetCount()):
            catalyzerInstance.MoveObjectToGroupAllowDuplicateNames(newSelectedObjects.GetObjectByIndex(i))
                 
        # Since we have just imported a file, update the signature information so we know when
        # we need to import it again.
        CatalyzerSourceChangeMonitor.UpdateFileSignature( catalyzerInstance, "ReferencedComponent" )       
    else:
        return False


    # Cause any sub-catalyzers to refresh.  
    # TODO: This should only trigger when needed by the properties, but that isn't what is happening right now.
    #for i in range(catalyzerInstance.Count()):
    #    obj = catalyzerInstance.GetObjectByIndex(i)
    #    RefreshCatalyzers(obj)
    
    startingDocument.MakeCurrentDocument()    
    startingDocument.ClearSelections()
    #TODO put back startingDocument.SelectObjects(originalSelection);    
            
            
def Catalyzer_DoMagnify( catalyzerInstance ):	
    referencedComponentName = catalyzerInstance.GetAttributeValueString( "ReferencedComponent" );
    if referencedComponentName:
        currDoc = Document.OpenDocument(os.path.abspath(referencedComponentName))
        if currDoc:
            currDoc.MakeCurrentDocument()
            
def Catalyzer_GetPreferredTypeName( ):
    return "GLS Ref"