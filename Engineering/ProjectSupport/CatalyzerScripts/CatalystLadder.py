# This file is licensed under the MIT License.
# 
# Copyright (c) 2015 by The DiSTI Corporation.
# 
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
# 
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
# 
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.

"""
A Catalyzer-friendly version of the script developed Python scripting tutorial 
in the GL Studio Cockpit Tutorial for C++. It exposes properties in the Object 
Properties table and effectively makes a new object type.
"""

import Catalyzer
from Vertex import Vertex
from VertexArray import VertexArray
from Group import Group
from GlsPolyLine import GlsPolyLine
from GlsTextGrid import GlsTextGrid
from glsColor import glsColor
import os


def AddLadder( group, basename, num_ticks=20, offset=50, width=200, position=Vertex( 0.0, 0.0, 0.0 ), ladderColor=glsColor( 255, 0, 0, 255 ), labels=None ):
    """
    Adds a full pitch ladder, complete with labels, to the .gls design.
    In: num_ticks (20): The number of tick marks in the ladder.
    offset (50): The offset between tick marks.
    width (200): The width of each tick.
    position (0,0,0): The starting position for the ladder
    labels ([]): A list of labels for each tick.
    """
    
    if not labels:
        labels = []
    adiLadder = Group( )
    adiLadder.SetName( basename )
    group.InsertObjectAllowDuplicateNames( adiLadder )
    for num in range( num_ticks ):
        vertArray = VertexArray( )
        vertArray.Insert( Vertex( position.x - (width / 2.0), 0.0, position.z ) )
        vertArray.Insert( Vertex( position.x + (width / 2.0), 0.0, position.z ) )
        line = GlsPolyLine( vertArray, Vertex( 0.0, position.y + (num * offset), 0.0 ) )
        line.FillColor( ladderColor )
        line.SetName( basename + "line" + str( num ) )
        adiLadder.InsertObjectAllowDuplicateNames( line )
        if len( labels ) > num and labels[num] is not None:
            label1 = GlsTextGrid( )
            label1.SetName( basename + "label1" + str( num ) )
            label1.Columns( len( labels[num] ) )
            label1.String( labels[num] )
            label1.SetPolygonMode( label1.POLY_MODE_POINTS )
            label2 = label1.DuplicateObject( )
            label2.SetName( basename + "label2" + str( num ) )
            vertList = VertexArray( )
            label1.GetVertices( vertList )
            label1.SetLocation(Vertex(position.x - (width/1.8) - (label1.Columns() * vertList.GetVertexAtIndex(2).x), position.y + (num * offset), position.z))
            label2.SetLocation( Vertex( position.x + (width / 1.8), position.y + (num * offset), position.z ) )
            adiLadder.InsertObjectAllowDuplicateNames( label1 )
            adiLadder.InsertObjectAllowDuplicateNames( label2 )


def Catalyzer_GetPropertiesList():
    return [
        ( "ticks0",       "int32",    "Ticks Inner",  __name__,  "Number of Ticks to use for the inner ladder",  10),
        ( "ticks1",       "int32",    "Ticks Outer",  __name__,  "Number of Ticks to use for the outer ladder",  11),
        ( "offset0",      "int32",    "Offset Inner", __name__,  "Offset Between Tick Marks, inner ladder",     100),
        ( "offset1",      "int32",    "Offset Outer", __name__,  "Offset Between Tick Marks, outer ladder",     100),
        ( "width0",       "int32",    "Width Inner",  __name__,  "Width of Ticks, inner ladder",                100),
        ( "width1",       "int32",    "Width Outer",  __name__,  "Width of Ticks, outer ladder",                200),
        ( "LadderColor",  "color4ub", "LadderColor",  __name__,  "Color of the Ladders",                        "0 0 255 255")
    ]


def Catalyzer_OnPropertyChanged( catalyzerInstance, propertyName ):
    # print "Got OnPropertyChanged for: " + catalyzerInstance.GetName() + ":" + propertyName
    pass


def Catalyzer_DoReaction( catalyzerInstance ):
    ticks0 = int( catalyzerInstance.GetAttributeValueString( "ticks0" ) )
    ticks1 = int( catalyzerInstance.GetAttributeValueString( "ticks1" ) )
    offset0 = int( catalyzerInstance.GetAttributeValueString( "offset0" ) )
    offset1 = int( catalyzerInstance.GetAttributeValueString( "offset1" ) )
    width0 = int( catalyzerInstance.GetAttributeValueString( "width0" ) )
    width1 = int( catalyzerInstance.GetAttributeValueString( "width1" ) )
    ladderColor = glsColor( *map( int, catalyzerInstance.GetAttributeValueString( "LadderColor" ).split( ' ' ) ) )

    AddLadder( catalyzerInstance, "ladder1", ticks0, offset0, width0, Vertex( 0.0, 50.0, 0.0 ), ladderColor )
    AddLadder( catalyzerInstance, "ladder2", ticks1, offset1, width1, Vertex( 0.0, 0.0, 0.0 ), ladderColor, ["-50", "-40", "-30", "-20", "-10", None, "10", "20", "30", "40", "50"] )


def Catalyzer_GetRecommendedRefreshMode():
    """
    The editor will decide what to do.  The higher number will win.
    REFRESH_ALWAYS        - whenever needsReaction becomes true
    REFRESH_EXPLICIT      - Must manually force the catalyzer to run scripts. 
    (Disabled)REFRESH_AT_GENERATION - WIll refresh at generation if needsReaction is true # GLS-5735 - Implement At Generation
    """
    return Catalyzer.REFRESH_ALWAYS


def Catalyzer_GetPreferredTypeName():
    return "PitchLadder"

# Uncomment if you want to let the user view this script with the magnify button.
# def Catalyzer_DoMagnify( catalyzerInstance ):
# fileName = os.path.splitext(__file__)[0] + ".py"
#    print "Opening notepad... " + fileName
#    os.system('start notepad.exe '+fileName)

