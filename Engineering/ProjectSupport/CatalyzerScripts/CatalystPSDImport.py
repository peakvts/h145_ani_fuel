# This file is licensed under the MIT License.
# 
# Copyright (c) 2015 by The DiSTI Corporation.
# 
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
# 
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
# 
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.

""" Catalyst - Imports from Adobe Photoshop files and updates with changes when the PSD file changes. """

from GlsPSDImporter import GlsPSDImporter

import os
import zlib
import ast
import Editor
import Catalyzer
from catalystUtil import CatalyzerSourceChangeMonitor


def Catalyzer_GetPropertiesList():
    # [ ( internalName, propType, displayName, groupName, description, defaultValue ), ]
    return [
    ( "PSDFile", "utf8_filepath", "Photoshop File", __name__, "The Photoshop file we want to import", "" ), 
    # This hidden property must exist and the name must follow this pattern to be used by the CatalyzerSourceChangeMonitor module
    ( "_PSDFile_LastUpdateSignature", "ascii", "Update Signature for PSD File", __name__, "", "" ), 
    ( "RepositionOnImport", "boolean", "Reposition on Import", __name__, "Move the geometry so it's bottom left extents are at 0,0", True ), 
    ]


def Catalyzer_DoReaction( catalyzerInstance ):
    doc = catalyzerInstance.GetDocument()

    importer = GlsPSDImporter.GetInstance()

    psdFileName = catalyzerInstance.GetAttributeValueString( "PSDFile" )

    if (0 == len( psdFileName ) or
        not os.path.isfile( psdFileName )):
        return False

    originalSelection = doc.GetSelectedObjects()
    
    doc.ClearSelections()
    
    # Note: Do not use backslashes here (even \\escaped ones).  They seem to get interpreted somewhere else in the pipeline.
    newGroup = importer.Import( psdFileName.replace('\\', '/'),
        catalyzerInstance.GetDocument(),
        True, # Generate new objects?
        int(catalyzerInstance.GetAttributeValueString( "RepositionOnImport" )),
        1,    # scale
        True) # force use of original name       
    CatalyzerSourceChangeMonitor.UpdateFileSignature( catalyzerInstance, "PSDFile" )
    
    newGroup.SetLocation(catalyzerInstance.GetLocation())
    
    while newGroup.Count():
        catalyzerInstance.MoveObjectToGroupAllowDuplicateNames(newGroup.GetObjectByIndex(0))
    
    # Remove the remaining empty group
    newGroup.Ungroup()

    # Put the selection back where we found it
    doc.SelectObjects( originalSelection )


def Catalyzer_OnScriptAssigned( catalyzerInstance ):
    CatalyzerSourceChangeMonitor.Start( catalyzerInstance, "PSDFile" )


def Catalyzer_OnScriptUnassigned( catalyzerInstance ):
    CatalyzerSourceChangeMonitor.Stop( catalyzerInstance, "PSDFile" )


def Catalyzer_OnPropertyChanged( catalyzerInstance, propertyName ):
    if "PSDFile" == propertyName:
        CatalyzerSourceChangeMonitor.Start( catalyzerInstance, "PSDFile" )


def Catalyzer_DoMagnify( catalyzerInstance ):    
    print "Got Catalyzer_DoMagnify; opening PSD ..."
    psdFileName = catalyzerInstance.GetAttributeValueString( "PSDFile" )
    if os.path.exists( psdFileName ):
        os.system('start ' + psdFileName)


def Catalyzer_GetRecommendedRefreshMode( ):
    return Catalyzer.REFRESH_ALWAYS


def Catalyzer_GetPreferredIcon():
    try:
        import ProjectConfiguration
        return os.path.join( ProjectConfiguration.current.iconsDir, "Photoshop_Icon.png" )
    except:
        pathToIcon = os.path.join(os.path.dirname(os.path.abspath(__file__)), "../Icons/Photoshop_Icon.png")
        if os.path.isfile( pathToIcon ):
            return pathToIcon
        return None


def Catalyzer_GetPreferredTypeName():
    return "PSDImport"

