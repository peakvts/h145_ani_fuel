# This file is licensed under the MIT License.
# 
# Copyright (c) 2015 by The DiSTI Corporation.
# 
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
# 
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
# 
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.

""" Catalyst - Creates a GlsNinePatch object based on the data from the .9.png.

http://developer.android.com/guide/topics/graphics/2d-graphics.html#nine-patch

NOTE: Only a single stretchable area is currently supported for vertical and a single for horizontal, however the .9.png format allows for multiple stretch areas per axis.

The padding box information is collected but not used here but this data can be used by other catalyzer scripts assigned to this object.
"""

import Catalyzer
import GlsNinePatch

import VertexArray
import Vector
import Vertex
import GLPolygon
from catalystUtil import CatalyzerSourceChangeMonitor
from catalystUtil import png
import os
import Editor
import EditorLog
import TexturePaletteEntry
import TexturePalette
import glsColor
import itertools
import ProjectConfiguration


def Catalyzer_GetPropertiesList( ):
    # [ ( internalName, propType, displayName, groupName, description, defaultValue ), ]
    return [
        ( "NinePatchFile", "utf8_filepath", "Nine Patch File", __name__, "The .9.png file we want to import", "" ),
        ( "PixelPerLogicalScale", "float32", "Scale", __name__, "Greater than 1 shrinks the edges", "1.0" ),
        # This hidden property must exist and the name must follow this pattern to be used by the CatalyzerSourceChangeMonitor module
        ( "_NinePatchFile_LastUpdateSignature", "ascii", "Update Signature for NinePatch File", __name__, "", "" ),
        ( "_NinePatchContentInset_Left", "int32", "Inset Left", __name__, "Content inset for use by chained catalyzers", "0" ),
        ( "_NinePatchContentInset_Right", "int32", "Inset Right", __name__, "Content inset for use by chained catalyzers", "0" ),
        ( "_NinePatchContentInset_Top", "int32", "Inset Top", __name__, "Content inset for use by chained catalyzers", "0" ),
        ( "_NinePatchContentInset_Bottom", "int32", "Inset Bottom", __name__, "Content inset for use by chained catalyzers", "0" ),
        
    ]

    
def _SaveContentInsets( catalyzerInstance, left, right, top, bottom ):
    # These are calculated in the script but not used here.
    # They are intended to be used by other Catalyzer scripts that run after this one, that 
    # might want to place content in the ideal location for this 9path object.
    catalyzerInstance.SetAttributeValueString( "_NinePatchContentInset_Left", str(left) )
    catalyzerInstance.SetAttributeValueString( "_NinePatchContentInset_Right", str(right) )
    catalyzerInstance.SetAttributeValueString( "_NinePatchContentInset_Top", str(top) )
    catalyzerInstance.SetAttributeValueString( "_NinePatchContentInset_Bottom", str(bottom) )
    
    
def _CopyAsCroppedImagePNG( originalFile, targetFile ):    
    anotherReader=png.Reader(originalFile)
    w, h, p, m = anotherReader.asRGBA8()
    
    # Get a row iterator with the first and last entries dropped
    smallerP = itertools.islice( p, 1, h -1 )
    # Turn it into a list
    pList = list(smallerP)
    # For each row, replace the provided iterator with a shortened one 
    for rowIndex, value in enumerate(pList):
        pList[rowIndex]=itertools.islice( value, 4, (w-1)*4 )
    
    # Write the file, making sure to tell it the size explicity since it can't easily figure it out now
    png.from_array(pList, mode='RGBA;8', info={'height':h-2, 'width':w-2}).save(targetFile)

    
def _Get9DotPngInsets( filename ):
    imageWidth, imageHeight, pixelsIterator, metadata = png.Reader(filename).asRGBA8()

    rowList = list(pixelsIterator)    
            
    def GetPixel( x, y ):
        flippedY = (len(rowList) - 1) - y
        row = rowList[flippedY]
        rowDataList = list(row)
        offset = x * 4 #RGBA
        return (rowDataList[offset], rowDataList[offset+1], rowDataList[offset+2], rowDataList[offset+3])
    
    def ColorEqual( a, b ):
        #print "comparing: ", a[0], a[1], a[2], a[3]
        #print "       to: ", b[0], b[1], b[2], b[3]
        return a[0] == b[0] and a[1] == b[1] and a[2] == b[2] and a[3] == b[3]

    def XYtoUV( x, y ):
        localU = (0.0 + x) / (imageWidth-1)
        localV = (0.0 + y) / (imageHeight-1)        
        return (localU, localV)
        
    def GetFirstBlackPixelCoords( rangeX, rangeY ):
        blackPixel = [0, 0, 0, 255]
        for x in rangeX:
            for y in rangeY:
                newPixel =  GetPixel( x, y );
                if ColorEqual(newPixel, blackPixel):
                    return (x,y)
        print "didn't find black pixel"
        return ( rangeX[0], rangeY[0] )
        
    def GetFirstDifferentPixelCoords( rangeX, rangeY ):
        comparePixel = GetPixel(rangeX[0], rangeY[0])
        for x in rangeX:
            for y in rangeY:
                print "checking: ", x,y
                if not ColorEqual(newPixel, comparePixel):
                    return (x,y)
        print "didn't find different pixel"
        return ( rangeX[0], rangeY[0] )

    # The insets of the bar on the top
    insetLeft = GetFirstBlackPixelCoords( range(0, imageWidth), [imageHeight-1] )[0]
    insetRight = (imageWidth - 1) - GetFirstBlackPixelCoords( range(imageWidth-1, -1, -1), [imageHeight-1] )[0]

    # The insets of the bar on the left side
    insetBottom = GetFirstBlackPixelCoords( [0], range( 0, imageHeight ) )[1]
    insetTop = (imageHeight - 1) - GetFirstBlackPixelCoords( [0], range( imageHeight-1, -1, -1 ) )[1]

    # The insets of the bar across the bottom
    contentLeft = GetFirstBlackPixelCoords( range(0, imageWidth), [0] )[0]
    contentRight = (imageWidth - 1) - GetFirstBlackPixelCoords( range(imageWidth-1, -1, -1), [0] )[0]

    # The insets of the bar on the right side
    contentBottom = GetFirstBlackPixelCoords( [imageWidth-1], range( 0, imageHeight ) )[1]
    contentTop = (imageHeight - 1) - GetFirstBlackPixelCoords( [imageWidth-1], range( imageHeight-1, -1, -1 ) )[1]
    
    # The -1 is because the calculated insets are based on the un-cropped image
    return ( insetLeft-1, insetRight-1, insetTop-1, insetBottom-1, contentLeft-1, contentRight-1, contentTop-1, contentBottom-1 )

    
def _GetPaletteEntryForFile( texturePalette, filename ):
    entry = None
    fileNameToCompare = os.path.abspath(filename)
    # If we already have the texture in the pallette, reload it    
    for t in range(texturePalette.GetCount()):
        e = texturePalette.GetTexturePaletteEntry(t)
        if not e.isNULL():
            if fileNameToCompare == os.path.abspath(e.GetFilename()):
                # Found it
                entry = e
                entry.Reload()
                break
    # Otherwise make a new one
    if not entry:
        entry = texturePalette.AddTexture(filename)
    return entry

    
def Catalyzer_DoReaction( catalyzerInstance ):
    doc = catalyzerInstance.GetDocument( )
    
    filename = catalyzerInstance.GetAttributeValueString( "NinePatchFile" )
    
    if not os.path.exists( filename ):
        EditorLog.PrintWarning("Could not find file: "+filename)
        return False # The file may have just gone missing so return False
    
    if not filename.lower().endswith('.9.png'):
        EditorLog.PrintError("This script is designed only for the .9.png format.  You can convert and existing image to this format easily using a tool such as this: https://romannurik.github.io/AndroidAssetStudio/nine-patches.html")
        
        return True # This is a permenant error, so we want an empty catalyzer.
        
    # Make sure the directory exists before we write to it
    if not os.path.exists( ProjectConfiguration.current.generatedTexturesDir ):
        os.makedirs( ProjectConfiguration.current.generatedTexturesDir )
        
    # Create another file which is the same but has the outside pixels cropped        
    generatedFilename = os.path.relpath( os.path.join(ProjectConfiguration.current.generatedTexturesDir, os.path.basename(filename) + "_1px_cropped.png"), doc.GetDirname())
    _CopyAsCroppedImagePNG(filename, generatedFilename);
            
    
    entry = _GetPaletteEntryForFile( doc.GetTexturePalette(), generatedFilename )

    insetLeft, insetRight, insetTop, insetBottom, contentLeft, contentRight, contentTop, contentBottom = _Get9DotPngInsets( filename )

    imageWidth = entry.GetSourceImageWidth();
    imageHeight = entry.GetSourceImageHeight();
        
    ninepatch = GlsNinePatch.GlsNinePatch()
    
    pixelPerLogicalScale = float(catalyzerInstance.GetAttributeValueString("PixelPerLogicalScale"))
    if pixelPerLogicalScale == 0.0:
        pixelPerLogicalScale = 1.0
    
    ninepatch.CenterVisible(True)
    ninepatch.VertexEdge( ninepatch.LEFT, insetLeft / pixelPerLogicalScale )
    ninepatch.TextureEdge( ninepatch.LEFT, insetLeft )
    
    ninepatch.VertexEdge( ninepatch.RIGHT, insetRight / pixelPerLogicalScale )
    ninepatch.TextureEdge( ninepatch.RIGHT, insetRight )
    
    ninepatch.VertexEdge( ninepatch.TOP, insetTop / pixelPerLogicalScale )
    ninepatch.TextureEdge( ninepatch.TOP, insetTop )
    
    ninepatch.VertexEdge( ninepatch.BOTTOM, insetBottom / pixelPerLogicalScale )
    ninepatch.TextureEdge( ninepatch.BOTTOM, insetBottom )
    
    ninepatch.TextureBLC( 0, 0 )
    ninepatch.TextureTRC( imageWidth, imageHeight )
    
    ninepatch.Width( imageWidth )
    ninepatch.Height( imageHeight )
    ninepatch.SetAttributeValueString("ReferenceTextureWidth", str(imageWidth))
    ninepatch.SetAttributeValueString("ReferenceTextureHeight", str(imageHeight))
    
    ninepatch.SetTextureIndex(entry.GetTextureIndex())

    ninepatch.SetAttributeValueString("TextureRepeat", "0")

    
    ninepatch.SetName("ninePatch")
    
    catalyzerInstance.InsertObjectAllowDuplicateNames(ninepatch, True)
           
    _SaveContentInsets( catalyzerInstance, contentLeft, contentRight, contentTop, contentBottom )  

    
def Catalyzer_GetRecommendedRefreshMode():
    return Catalyzer.REFRESH_ALWAYS

    
def Catalyzer_OnPropertyChanged( catalyzerInstance, propertyName ):
    if "NinePatchFile" == propertyName:
        CatalyzerSourceChangeMonitor.Start( catalyzerInstance, "NinePatchFile" )

        
def Catalyzer_GetPreferredTypeName():
    return "NinePatch"
