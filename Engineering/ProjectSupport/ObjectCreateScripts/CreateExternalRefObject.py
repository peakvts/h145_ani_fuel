# This file is licensed under the MIT License.
# 
# Copyright (c) 2015 by The DiSTI Corporation.
# 
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
# 
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
# 
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.

""" GLS Action - Creates a Catalyzer that refers to an external GLS file. """
import Document
import Group
import Catalyzer
import os
import threading
import Editor
import ProjectConfiguration

doc = Document.GetCurrentDocument()
if doc:   
    Document.OpenDocument("")
    docToReference = Document.GetCurrentDocument()
    filenameToReference = os.path.join(docToReference.GetDirname(), docToReference.GetFilename())                          
    if not docToReference == doc and filenameToReference:    
        catalyzer = Catalyzer.Catalyzer()
        catalyzer.AddScript(os.path.join(ProjectConfiguration.current.catalyzerScriptsDir, "CatalystComponentObject.py") )
        catalyzer.SetName(os.path.basename(filenameToReference).split('.')[0])       
        doc.InsertObject(catalyzer, True)
        print "Setting referenced component to:", filenameToReference
        catalyzer.SetAttributeValueString("ReferencedComponent", filenameToReference)
        
        doc.MakeCurrentDocument()
    else:
        print 'Document must have failed to open.'
