# This file is licensed under the MIT License.
# 
# Copyright (c) 2015 by The DiSTI Corporation.
# 
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
# 
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
# 
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.

""" GLS Action - Create a Catalyzer that refers to a locally selected file. """

import Document
import Group
import GlsTextBox
import Editor
import os
import DisplayObject
import DisplayObjectArray
import Catalyzer
import EditorLog
import ProjectConfiguration


doc = Document.GetCurrentDocument()
if doc:
    # Save the initial selection
    originalSelection = doc.GetSelectedObjects()
    doc.ClearSelections()
    originalSelectionNames = []
    for i in range(originalSelection.GetCount()):
        originalSelectionNames.append( originalSelection.GetObjectByIndex( i ).GetName() )
        
    if not originalSelectionNames:
        EditorLog.PrintWarning("Select objects to reference");
    else:
        # Setup a new catalyzer to refer to the contents of the new file
        catalyzer = Catalyzer.Catalyzer()
        # Determine a reasonable object name
        refName = originalSelectionNames[0] + "Ref"
        catalyzer.SetName(refName)        
        doc.InsertObject(catalyzer, True)
        catalyzer.AddScript( ProjectConfiguration.current.RelativeToDoc( doc, 'catalyzerScriptsDir', 'CatalystLocalObject.py' ) )        
        
        catalyzer.SetAttributeValueString("ReferencedObjects", ' '.join(map(str,originalSelectionNames)) )    

        doa = DisplayObjectArray.DisplayObjectArray()
        doa.Insert( catalyzer )
        doc.SelectObjects( doa )
    
