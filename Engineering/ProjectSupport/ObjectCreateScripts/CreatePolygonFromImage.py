# This file is licensed under the MIT License.
# 
# Copyright (c) 2015 by The DiSTI Corporation.
# 
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
# 
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
# 
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.

""" Creates a GL Polygon sized to fit the specified texture file """

import os
import string
import time
import traceback
import Editor
import Catalyzer
from catalystUtil import CatalyzerSourceChangeMonitor
import Vector
import Vertex
import VertexArray
import DisplayObject
import GLPolygon
import EditorLog
import Document
import Group
import ProjectConfiguration
import shutil


def CreatePolygon( doc, textureFileName ):
        
    texturePalette = doc.GetTexturePalette()
    entry = None

    # If we already have the texture in the palette, reload it
    for t in range(texturePalette.GetCount()):
        e = texturePalette.GetTexturePaletteEntry(t)
        if not e.isNULL() and os.path.abspath(e.GetFilename()) == os.path.abspath(textureFileName):
            # Found it
            entry = e
            entry.Reload()
            break
            
    # Otherwise make a new one
    try:
        if not entry:
            entry = texturePalette.AddTexture(textureFileName)        
    except:
        entry = None

    if not entry:
        EditorLog.PrintError( "Unable to load texture %s" % textureFileName )
        return None            
    
    width  = entry.GetSourceImageWidth()
    height = entry.GetSourceImageHeight()
    
    # Size the box from the image size
    verts = VertexArray.VertexArray()
    verts.Insert(Vertex.Vertex(0,     0,      0))
    verts.Insert(Vertex.Vertex(width, 0,      0))
    verts.Insert(Vertex.Vertex(width, height, 0))
    verts.Insert(Vertex.Vertex(0,     height, 0))
    obj = GLPolygon.GLPolygon(verts, Vertex.Vertex(0,0,0))   

    obj.SetTextureIndex(entry.GetTextureIndex())
    
    texPoints = VertexArray.VertexArray()
    texPoints.Insert(Vertex.Vertex(width, 0,      0))
    texPoints.Insert(Vertex.Vertex(0,     0,      0))
    texPoints.Insert(Vertex.Vertex(0,     height, 0))
    texPoints.Insert(Vertex.Vertex(width, height, 0))
    
    obj.SetTexturePoints(texPoints)
    obj.SetPolygonMode(obj.POLY_MODE_FILLED)
    
    return obj

    
def CreateObjectFromFile( imageFileName ):
    try:
        if (0 == len( imageFileName ) or
            not os.path.isfile( imageFileName )):
            return False

        if os.path.abspath(ProjectConfiguration.current.generatedTexturesDir) in os.path.abspath( imageFileName ):
            EditorLog.PrintWarning("Do not reference textures directly from '"+ProjectConfiguration.current.generatedTexturesDir+"'.\n"+
                                   "Place them here instead: " + ProjectConfiguration.current.texturesDir)
            return False

        if os.path.abspath(ProjectConfiguration.current.projectDir) not in os.path.abspath( imageFileName ):
            EditorLog.PrintInfo("Copying " + os.path.abspath(imageFileName) + " to the project directory (" + ProjectConfiguration.current.texturesDir + ").")       
            shutil.copy(os.path.abspath(imageFileName), ProjectConfiguration.current.texturesDir)
            imageFileName = ProjectConfiguration.current.texturesDir + "/" + os.path.abspath(imageFileName).split('/')[-1].split('\\')[-1]           
        
        doc = Document.GetCurrentDocument()
        if doc:
            try:
                relativeImageFileName = os.path.relpath( imageFileName, doc.GetDirname() )
                newName               = os.path.splitext(os.path.basename( imageFileName ))[0]
            except:
                EditorLog.PrintError( "Unable to create image.  Is it in the same drive as the project?")
                return False

            # Try to force the filename to a valid C identifier
            validChars = "_%s%s" % (string.ascii_letters, string.digits)
            newName = ''.join(x for x in newName if x in validChars)
            if len(newName) == 0: # or newName[0] in string.digits:
                newName = '_' + newName
                
            newPoly = CreatePolygon( doc, imageFileName )
            if newPoly:
                newPoly.SetName( newName )
                
                # Insert into the first group that is not a catalyzer
                selection = doc.GetSelectedObjects()
                inserted = False
                if selection.GetCount() != 0:
                    obj = selection.GetObjectByIndex( selection.GetCount() - 1 )
                    while obj is not None:
                        parent = obj.GetParent()
                        if Catalyzer.CastToCatalyzer( parent ).isNULL():
                            parentGroup = Group.CastToGroup( parent ) 
                            parentGroup.InsertObject( newPoly, True, True, Group.Group.DEFAULT_GROUP_LOCATION, True )
                            inserted = True
                            break
                        obj = parent
                # If all else fails....
                if not inserted:
                    doc.InsertObject( newPoly, True )
                    
                EditorLog.PrintInfo("Created " + newPoly.GetName() )        
                
        else:
            EditorLog.PrintInfo("No doc")
            
        Editor.ForceEditorRefresh()
    except:
        print "Unexpected error:", traceback.print_exc()
