# This file is licensed under the MIT License.
# 
# Copyright (c) 2015 by The DiSTI Corporation.
# 
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
# 
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
# 
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.

""" GLS Action - Moves selected objects to an external GLS file and replaces them with a reference to that file. """

import Document
import Group
import GlsTextBox
import os
import DisplayObject
import Catalyzer
import Editor
import ProjectConfiguration

startingDocument = Document.GetCurrentDocument()
if startingDocument:
    originalSelection = startingDocument.GetSelectedObjects()
    startingDocument.CopySelectedObjects()
    
    newDoc = Document.NewDocument()
    newDoc.PasteClipboardObjects( True, True )
    
    # Cause the Save As popup to appear
    try:
        newDoc.SaveAs(None)
    except:
        newDoc.Close()
        return

    # Grab the name of the new gls file
    newFileName = newDoc.GetFilename()
    newDirName = newDoc.GetDirname()

    # Switch back to our original document
    startingDocument.MakeCurrentDocument()
    
    # Setup a new catalyzer to refer to the contents of the new file
    catalyzer = Catalyzer.Catalyzer()
    catalyzer.SetName(os.path.basename(newFileName).split('.')[0])
    startingDocument.InsertObject(catalyzer, True)
    catalyzer.AddScript(os.path.join(ProjectConfiguration.current.catalyzerScriptsDir, "CatalystComponentObject.py") )
    
    fullName = os.path.join(newDirName, newFileName);
    catalyzer.SetAttributeValueString("ReferencedComponent", os.path.relpath(fullName, startingDocument.GetDirname()))  

    # Remove the original objects
    startingDocument.ClearSelections()
    startingDocument.SelectObjects( originalSelection )
    startingDocument.DeleteSelectedObjects()

