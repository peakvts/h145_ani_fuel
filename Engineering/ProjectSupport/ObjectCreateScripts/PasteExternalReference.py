# This file is licensed under the MIT License.
# 
# Copyright (c) 2015 by The DiSTI Corporation.
# 
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
# 
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
# 
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.

""" GLS Action - Paste a previously copied reference as an external reference. """

import Document
import Group
import Catalyzer
import os
import threading
import Editor
import EditorLog
import ProjectConfiguration

# Expected to contain a tuple with two strings ("name of document", "selected objects")
foundCopyData = None
try:
    foundCopyData = Editor.externalReferenceCopyData
except:
    pass

if not foundCopyData:
    EditorLog.PrintWarning("Nothing copied; please activate the CopyReferenceForExternal script to identify the copy source.")
    return
    
doc = Document.GetCurrentDocument()
if doc:   
    filenameToReference = foundCopyData[0]
    thisDocFileName = os.path.join(doc.GetDirname(), doc.GetFilename())
    
    if os.path.abspath(thisDocFileName) == os.path.abspath(filenameToReference):
        # TODO: It would be possible here to automatically create a local reference instead of just providing an error
        EditorLog.PrintWarning("Please paste into a different file, or create a local reference instead.  Paste ignored.")
        return
        
    if filenameToReference:    
        catalyzer = Catalyzer.Catalyzer()
        catalyzer.AddScript(os.path.join(ProjectConfiguration.current.catalyzerScriptsDir, "CatalystComponentObject.py") )
        catalyzer.SetName(os.path.basename(filenameToReference).split('.')[0])       
        doc.InsertObject(catalyzer, True)
        catalyzer.SetAttributeValueString("ReferencedComponent", filenameToReference)
        catalyzer.SetAttributeValueString("ReferencedObjects", foundCopyData[1])        
        EditorLog.PrintInfo("External reference created.")
        
    else:
        print 'Document must have failed to open.'
