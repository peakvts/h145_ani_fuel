# This file is licensed under the MIT License.
# 
# Copyright (c) 2015 by The DiSTI Corporation.
# 
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
# 
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
# 
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.

""" Sets up a web server on the specified port and serves specified directories and logs POSTs to stdio.html """

import Editor
import EditorLog
import SimpleHTTPServer
import BaseHTTPServer
import SocketServer
import threading
import cgi
import urllib2
import ProjectConfiguration
import socket

#### Utility
# Put our request into the mainThreadQueue and wait for a result
def CallOnMainThreadAndWait(func, *args, **keywords):
    class local:   # Provide a namespace to access rval from callIt()  See: http://stackoverflow.com/questions/5218895/python-nested-functions-variable-scoping
        rval = None
    def CallIt():
        local.rval = func(*args, **keywords)
        
    if not Editor.shuttingDownEvent.isSet():
        Editor.mainThreadQueue.put(CallIt)
        Editor.mainThreadQueue.join()
    return local.rval
    
def CallOnMainThread(func, *args, **keywords):
    def CallIt():
        func(*args, **keywords)        
    Editor.mainThreadQueue.put(CallIt)
   
class Server:   
    _thisServer = None   
    def __init__(self, port, projectConfig):
        global _thisServer # "global" is just allowing access to the attribute in the containing class
        _thisServer = self

        self.additionalGetHandlers = dict()
        
        self.port = port
        self.projectConfig = projectConfig

    # Specify a custom handler to be called for both GET and POST requests
    # Provide a handler of the form
    #   def someFunction( s ):
    #       if s is a path we handle:
    #          return True
    #       else:
    #          return False
    def AddGetHandler( self, uniqueId, function ):
        self.additionalGetHandlers[ uniqueId ] = function
        
    def RemoveGetHandler( self, uniqueId, function ):
        del self.additionalGetHandlers[ uniqueId ] 
        
    class CustomHandler(SimpleHTTPServer.SimpleHTTPRequestHandler):       
        def _DoGetIfPathValid(s):
            """ If a path has been specified in the mapping, a GET will be processed for it, otherwise ignored """
            for f in _thisServer.additionalGetHandlers:
                if _thisServer.additionalGetHandlers[f]( s ):
                    return
                    
            for e in _thisServer.projectConfig.webPortal.pathMapping:
                if s.path.startswith(e):
                    s.path = s.path.replace(e, _thisServer.projectConfig.webPortal.pathMapping[e], 1)
                    SimpleHTTPServer.SimpleHTTPRequestHandler.do_GET(s)
                    return
                    
        # override
        def log_request(self, code):
            # Filter out 200 OK messages to remove noise.
            if code is not 200:
                SimpleHTTPServer.SimpleHTTPRequestHandler.log_request(self, code)
                
        # override
        def translate_path(self, path): 
            return path # Translation is done in _DoGetIfPathValid before the call to get            
            
        # override   
        def do_HEAD(s):  
            s.send_response(200)
            s.send_header("Content-type", "text/html")
            s.end_headers()
        
        # override
        def do_POST(s):  
            if s.path.endswith("/stdio.html"):
                length = int(s.headers['Content-length'])
                content = s.rfile.read(length)
                # This is a way for a web page to push stderr and stdout data back to show in the GL Studio log
                s.send_response(200)
                s.send_header("Content-type", "text/html")
                s.end_headers()
                try:
                    # Format is ^out^23^Some content to the end
                    # The 23 is an incrementing index which is not being used here.  'out' is 'err' for stderr.
                    splitContent = content.split("^",4)
                    if splitContent[1]=="err":
                        CallOnMainThread(EditorLog.PrintWarning, "RUNTIME(port:"+str(_thisServer.port)+"): " + splitContent[3]);
                    else:
                        CallOnMainThread(EditorLog.PrintInfo, "RUNTIME(port:"+str(_thisServer.port)+"): " + splitContent[3]);
                except:
                    pass

            else:
                s._DoGetIfPathValid()
            
        # override
        def do_GET(s):
            """Respond to a GET request."""
            s._DoGetIfPathValid()
                
        
    def Start(self):
        self.handler = self.CustomHandler

        # Notice that this only binds to "localhost".  This means that this server cannot be
        # accessed from off this box.  If you need external access you can set to "", but there
        # are security implications because then anybody on the network can access your running GL Studio.
        self.httpd = SocketServer.TCPServer(("localhost", self.port), self.handler)

        self.server_thread = threading.Thread(target=self.httpd.serve_forever)
        # Exit the server thread when the main thread terminates
        self.server_thread.daemon = True
        print "Starting http server listing on port:", self.port
        self.server_thread.start()

            
    def Stop(self):    
        def shutItDown():       
            if hasattr(self, 'httpd'):
                print "Stopping http server on port:",self.port,"..."
                self.httpd.shutdown()
                self.httpd.server_close()
                print "Closed http server on port:",self.port
            
        # Do shutdown in a seperate thread to avoid a deadlock with the editor
        shutdownThread = threading.Thread(target=shutItDown)
        shutdownThread.daemon = True
        shutdownThread.start()

def StartForCurrentProject():
    try:
        # We need to make sure that an instance of this Server class survives a reload of this module.  
        # This is because if we have an orphaned server running, it would block the port we need.
        #
        # To make sure our object survives a reload, we have to store it somewhere that doesn't get reloaded.
        # The Editor object does not reload.  So that is where we store it.
        #
        # As a consequence of this setup, if you want to make changes to this file, you will need to shutdown 
        # and restart the GL Studio editor for the changes to take effect.
        port = ProjectConfiguration.current.webPortal.port
        if not hasattr(Editor, "_localWebServerInstanceStorageByPort"):
            setattr( Editor, "_localWebServerInstanceStorageByPort", dict() )

        # Start the server if it hasn't yet been started
        if not port in Editor._localWebServerInstanceStorageByPort:
            EditorLog.PrintInfo("Local web server listening on port: " + str(port));
            newServer = Server( ProjectConfiguration.current.webPortal.port, ProjectConfiguration.current ) 
            Editor._localWebServerInstanceStorageByPort[ port ] = newServer
            newServer.Start()
            
    except socket.error:        
        EditorLog.PrintWarning("Error starting Web Portal, the socket is probably already in use on port "+str(ProjectConfiguration.current.webPortal.port)+". ("+__file__ + ")" )
        pass
        
# Returns the current server on the current port
def CurrentServer():
    port = ProjectConfiguration.current.webPortal.port
    if not hasattr(Editor, "_localWebServerInstanceStorageByPort"):
        return None
    else:
        return Editor._localWebServerInstanceStorageByPort[ port ]
