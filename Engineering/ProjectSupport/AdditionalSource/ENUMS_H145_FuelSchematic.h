#include <string>

// Fuel Quantity sensor
typedef enum FUEL_QUANTITY_SENSOR_STATUS_t
{
	FUEL_QUANTITY_OFF,
	FUEL_QUANTITY_OPERATIONAL,
	FUEL_QUANTITY_FAILED
} FUEL_QUANTITY_SENSOR_STATUS_t;
// implemented streaming operator for new type
inline std::ostream & operator<<(std::ostream & outstr, const FUEL_QUANTITY_SENSOR_STATUS_t & val)

