/** \file
  * \brief Utility functions for this project
  */
  
//-----------------------------------------------------------------------------
// This file is licensed under the MIT License.
// 
// Copyright (c) 2015 by The DiSTI Corporation.
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
//-----------------------------------------------------------------------------

#ifndef INCLUDED_PROJECT_UTIL_H
#define INCLUDED_PROJECT_UTIL_H

#ifdef WIN32
#pragma warning( push ) 
#pragma warning( disable: 4503 ) 
#endif

#include <utility> // For std::move(), where available
#include <string>
#include <sstream>
#include <vector>

#ifdef WIN32
#pragma warning( pop )
#endif

namespace disti
{
    namespace Util
    {
        // The Detail namespace is here to hide implementation details, mostly for things 
        // that are standard but not available on all supported platforms.
        namespace Detail
        {
            /// Replacement for std::is_convertible, adapted from Loki 0.1.7.
            /////////////////////////////////////////////////////////////////////////////////
            /// \copyright The Loki Library, Copyright (c) 2001 by Andrei Alexandrescu<br />
            /// This code accompanies the book: 
            /// Alexandrescu, Andrei. "Modern C++ Design: Generic Programming and Design 
            ///     Patterns Applied". Copyright (c) 2001. Addison-Wesley.<br />
            /// Permission to use, copy, modify, distribute and sell this software for any 
            ///     purpose is hereby granted without fee, provided that the above copyright 
            ///     notice appear in all copies and that both that copyright notice and this 
            ///     permission notice appear in supporting documentation.<br />
            /// The author or Addison-Welsey Longman make no representations about the 
            ///     suitability of this software for any purpose. It is provided "as is" 
            ///     without express or implied warranty.<br />
            /////////////////////////////////////////////////////////////////////////////////
            template <class T, class U>
            struct is_convertible
            {
            private:
                typedef char Small;
                struct Big { char dummy[2]; };
                static Big   Test(...);
                static Small Test(U);
                static T MakeT();
            public:
                enum { value = sizeof(Small) == sizeof((Test(MakeT()))) };
            };

            template <class T> struct is_convertible<T,    T   > { enum { value = true  }; }; //< Replacement for std::is_convertible, adapted from Loki 0.1.7.
            template <class T> struct is_convertible<void, T   > { enum { value = false }; }; //< Replacement for std::is_convertible, adapted from Loki 0.1.7.
            template <class T> struct is_convertible<T,    void> { enum { value = false }; }; //< Replacement for std::is_convertible, adapted from Loki 0.1.7.
            template<>         struct is_convertible<void, void> { enum { value = true  }; }; //< Replacement for std::is_convertible, adapted from Loki 0.1.7.
            
            /// Helper class for static checking. Adapted from Loki 0.1.7.
            template<int> struct CompileTimeError;
            template<> struct CompileTimeError<true> {};
        } // namespace Detail

        ////////////////////////////////////////////////////////////////////////////////
        /// Compile-time condition checking -- poor man's version of C++11's static_assert()
        /// \param expr a compile-time integral or pointer expression
        /// \param id   a C++ identifier that does not need to be defined
        /// \note If expr is zero, id will appear in a compile-time error message.
        /// \note Adapted from Loki 0.1.7.
        /// \note This macro appears here for grouping with like code, but macros 
        ///       aren't scoped to namespace.
        ////////////////////////////////////////////////////////////////////////////////
        #define DISTI_STATIC_ASSERT(expr, msg) \
            { disti::Util::Detail::CompileTimeError<((expr) != 0)> ERROR_##msg; (void)ERROR_##msg; } 
        
        ////////////////////////////////////////////////////////////////////////////////
        /// Convenience macro to check that one type converts to another at compile-time.
        /// \param T A class to check
        /// \param ConvertsTo A class to test T* against to verify that it can be 
        ///                   converted to ConvertsTo* without a cast.
        /// \note This macro appears here for grouping with like code, but macros 
        ///       aren't scoped to namespace.
        ////////////////////////////////////////////////////////////////////////////////
        #define DISTI_STATIC_ASSERT_IS_CONVERTIBLE_TO( T, ConvertsTo ) \
             DISTI_STATIC_ASSERT( (disti::Util::Detail::is_convertible< T*, ConvertsTo* >::value), \
                                  class_does_not_inherit_from_ ## ConvertsTo )

        ////////////////////////////////////////////////////////////////////////////////
        /// Macros for portability with standard C++ features across different 
        /// compilers (VS2012 and up and GCC 4.6 and up).
        ////////////////////////////////////////////////////////////////////////////////
        #if _MSC_VER >= 0x1700
        #    define DISTI_RVAL_MOVE(x) std::move(x)
        #    define DISTI_METHOD_OVERRIDE override
        #elif (__GNUC__ > 4 || (__GNUC__ == 4 && __GNUC_MINOR__ >= 6)) && ( defined(__GXX_EXPERIMENTAL_CXX0X__) || __cplusplus >= 201103L )
        #    define DISTI_RVAL_MOVE(x) std::move(x)
        #    define DISTI_METHOD_OVERRIDE
        #else
        #    define DISTI_RVAL_MOVE(x) (x)
        #    define DISTI_METHOD_OVERRIDE
        #endif

        ////////////////////////////////////////////////////////////////////////////////
        /// A macro for boilerplate code to setup a DisplayObject callback.
        /// \param instance The DisplayObject instance.
        /// \param Class The current class name, derived from DisplayObject.
        /// \param Method The class's method name for the callback.
        /// \code{.cpp}
        ///    // Given a class ScrollList that inherits from DisplayObject...
        ///    ScrollList::ScrollList()
        ///    {
        ///        DISTI_REGISTER_METHOD_CALLBACK( this, &ScrollList::EventCallback );
        ///    }
        ///
        ///    int ScrollList::EventCallback( Group* self, DisplayEvent* ev )
        ///    {
        ///         // ... event handler code ...
        ///    }
        /// \endcode
        ////////////////////////////////////////////////////////////////////////////////
        #define DISTI_REGISTER_METHOD_CALLBACK( instance, Class, Method )            \
            {                                                                        \
                typedef CallbackMethodCallerTemplate< Class, Class > Caller;         \
                typedef typename Caller::MethodType1                 MethodType;     \
                const MethodType callback = reinterpret_cast<MethodType>( Method );  \
                ThisClass::CallbackCaller( new Caller( callback, instance ) );       \
            }
        
        ////////////////////////////////////////////////////////////////////////////////
        /// A macro for boilerplate code to setup a DisplayObject method caller for 
        /// MethodType 1.
        /// \param object The DisplayObject instance.
        /// \param method The method that should be called.
        /// \param parent The parent to the object
        ////////////////////////////////////////////////////////////////////////////////
        #define DISTI_SET_OBJECT_CALLBACK1( object, method, parent )                                \
            (object)->CallbackCaller(                                                               \
                new CallbackMethodCallerTemplate< DisplayObject, DisplayObject >(                   \
                ( disti::CallbackMethodCallerTemplate<DisplayObject, DisplayObject>::MethodType1 )  \
                (method), (parent) ) );                                                             
            
        ////////////////////////////////////////////////////////////////////////////////
        /// A macro for boilerplate code to setup a DisplayObject attribute.
        /// \param Type The type of the attribute.
        /// \param name The unique string name identifying the attribute.
        /// \param memberVarAddr A pointer to the member variable of type \a Type. 
        ///                      Can be NULL if no member variable is desired.
        ////////////////////////////////////////////////////////////////////////////////
        #define DISTI_ADD_ATTRIBUTE( Type, name, memberVarAddr ) \
            { \
                 static const AttributeName attr( name ); \
                 this->Attributes().Add( new DistiAttribute<Type>(NULL, attr, memberVarAddr ) ); \
            }
            
        ////////////////////////////////////////////////////////////////////////////////
        /// A macro for boilerplate code to setup a string as a DisplayObject attribute.
        /// \param name The unique string name identifying the attribute.
        /// \param memberVarAddr A pointer to the member variable of type std::string. 
        ///                      Can be NULL if no member variable is desired.
        ////////////////////////////////////////////////////////////////////////////////
        #define DISTI_ADD_ATTRIBUTE_STRING( name, memberVarAddr ) \
            { \
                 static const AttributeName attr( name ); \
                 this->Attributes().Add( new DistiAttributeEncodedStdString( NULL, attr, memberVarAddr ) ); \
            }
            
        ////////////////////////////////////////////////////////////////////////////////
        /// A macro for boilerplate code to setup a string as a DisplayObject attribute
        /// with a callback
        /// \param name The unique string name identifying the attribute.
        /// \param memberVarAddr A pointer to the member variable of type std::string. 
        ///                      Can be NULL if no member variable is desired.
        /// \param callbackFunction The function that gets called when the attribute changes.
        ////////////////////////////////////////////////////////////////////////////////
        #define DISTI_ADD_ATTRIBUTE_STRING_CALLBACK( name, memberVarAddr, callbackFunction ) \
            { \
                typedef void (ThisClass::*Method)(); /* Member function pointer */ \
                CallbackMethodCallerTemplate<ThisClass> cb( reinterpret_cast<Method>( callbackFunction ),  this ); \
                static const AttributeName attr( name ); \
                this->Attributes().Add( new DistiAttributeEncodedStdString( &cb, attr, memberVarAddr ) ); \
            }
        ////////////////////////////////////////////////////////////////////////////////
        /// Clamps a value to the range [min, max]
        /// \param val The value to clamp
        /// \param min The minimum value in the range
        /// \param max The maximum value in the range
        ////////////////////////////////////////////////////////////////////////////////
        template<class T>
        T Clamp( const T& val, const T& min, const T& max )
        {
            return std::min( max, std::max( min, val ) );
        }
        
        ////////////////////////////////////////////////////////////////////////////////
        /// Split a string and add to existing vector of elements with delimeters removed.
        /// \param s The string to split
        /// \param delim The delimiter to split the string on.
        /// \param elems The vector of elements to append each split string to.
        /// \param maxElems The maximum number of elements to split. Remaining elements 
        ///                 are appended unsplit as the last value. If 0, it does not 
        ///                 have a maximum.
        /// \note Adapted from http://stackoverflow.com/questions/236129/split-a-string-in-c
        ////////////////////////////////////////////////////////////////////////////////
        inline void Split( const std::string& s, const char delim, std::vector<std::string>& elems, const std::size_t maxElems = 0 ) 
        {
            std::istringstream ss( s );
            std::string item;
            while( std::getline( ss, item, delim ) )
            {
                elems.push_back( DISTI_RVAL_MOVE( item ) );
                if( elems.size() == maxElems )
                {
                    break;
                }
            }
            if( elems.size() == maxElems && ss.good() && maxElems > 0 )
            {
                std::string remainder;
                std::getline( ss, remainder, '\0' );
                elems.back() += delim + remainder;
            }
        }

        ////////////////////////////////////////////////////////////////////////////////
        /// Split a string return a vector of the elements with delimeters removed.
        /// \param s The string to split
        /// \param delim The delimiter to split the string on.
        /// \param maxElems The maximum number of elements to split. Remaining elements 
        ///                 are appended unsplit as the last value. If 0, it does not 
        ///                  have a maximum.
        /// \return The vector of split elements.
        /// \note Adapted from http://stackoverflow.com/questions/236129/split-a-string-in-c
        ////////////////////////////////////////////////////////////////////////////////
        inline std::vector<std::string> Split( const std::string& s, const char delim, const std::size_t maxElems = 0 ) 
        {
            std::vector<std::string> elems;
            Split( s, delim, elems, maxElems );
            return DISTI_RVAL_MOVE( elems );
        }
        
        // Passing null to std::string is not well-defined. Create a string safely.
        inline std::string MakeString( const char* const cStr )
        {
            return ( cStr ? cStr : "" );
        }
    } // namespace Util
} // namespace disti

#endif