#include <string>

// Fuel low level sensor
typedef enum LOW_LVL_SENSOR_STATUS_t
{
    LOW_LVL_OFF,
    LOW_LVL_OPERATIONAL,
    LOW_LVL_LOW_FUEL,
    LOW_LVL_FAILED
} LOW_LVL_SENSOR_STATUS_t;

// implemented streaming operator for new type
inline std::ostream & operator<<(std::ostream & outstr, const LOW_LVL_SENSOR_STATUS_t & val)
{
    outstr << (int)val;
    return outstr;
}
 
inline std::istream & operator>>(std::istream & instr, LOW_LVL_SENSOR_STATUS_t & val)
{
       std::string temp;
       instr >> temp;
       val = (LOW_LVL_SENSOR_STATUS_t)atoi(temp.c_str());
    return instr;
}

// Fuel Quantity sensor
typedef enum FUEL_QUANTITY_SENSOR_STATUS_t
{
	FUEL_QUANTITY_OFF,
	FUEL_QUANTITY_OPERATIONAL,
	FUEL_QUANTITY_FAILED
} FUEL_QUANTITY_SENSOR_STATUS_t;

// implemented streaming operator for new type
inline std::ostream & operator<<(std::ostream & outstr, const FUEL_QUANTITY_SENSOR_STATUS_t & val)
{
    outstr << (int)val;
    return outstr;
}
 
inline std::istream & operator>>(std::istream & instr, FUEL_QUANTITY_SENSOR_STATUS_t & val)
{
       std::string temp;
       instr >> temp;
       val = (FUEL_QUANTITY_SENSOR_STATUS_t)atoi(temp.c_str());
    return instr;
}

// Fuel Quantity sensor
typedef enum FUEL_FLOW_SENSOR_STATUS_t
{
	FUEL_FLOW_OFF,
	FUEL_FLOW_OPERATIONAL,
	FUEL_FLOW_FAILED
} FUEL_FLOW_SENSOR_STATUS_t;

// implemented streaming operator for new type
inline std::ostream & operator<<(std::ostream & outstr, const FUEL_FLOW_SENSOR_STATUS_t & val)
{
    outstr << (int)val;
    return outstr;
}
 
inline std::istream & operator>>(std::istream & instr, FUEL_FLOW_SENSOR_STATUS_t & val)
{
       std::string temp;
       instr >> temp;
       val = (FUEL_FLOW_SENSOR_STATUS_t)atoi(temp.c_str());
    return instr;
}


// Fuel pump enum
typedef enum FUEL_PUMP_STATUS_t
{
	FUEL_PUMP_OFF,
	FUEL_PUMP_OPERATIONAL,
	FUEL_PUMP_FAILED
} FUEL_PUMP_STATUS_t;

// implemented streaming operator for new type
inline std::ostream & operator<<(std::ostream & outstr, const FUEL_PUMP_STATUS_t & val)
{
    outstr << (int)val;
    return outstr;
}
 
inline std::istream & operator>>(std::istream & instr, FUEL_PUMP_STATUS_t & val)
{
       std::string temp;
       instr >> temp;
       val = (FUEL_PUMP_STATUS_t)atoi(temp.c_str());
    return instr;
}


// Fuel pipe enum
typedef enum FUEL_PIPE_STATUS_t
{
	FUEL_PIPE_OFF,
	FUEL_PIPE_OPERATIONAL,
	FUEL_PIPE_PRESSURE
} FUEL_PIPE_STATUS_t;

// implemented streaming operator for new type
inline std::ostream & operator<<(std::ostream & outstr, const FUEL_PIPE_STATUS_t & val)
{
    outstr << (int)val;
    return outstr;
}
 
inline std::istream & operator>>(std::istream & instr, FUEL_PIPE_STATUS_t & val)
{
       std::string temp;
       instr >> temp;
       val = (FUEL_PIPE_STATUS_t)atoi(temp.c_str());
    return instr;
}

typedef enum FUEL_OVERFLOW_CHANNEL_t
{
	FUEL_OVERFLOW_OFF,
	FUEL_OVERFLOW_FLOW_LEFT,
	FUEL_OVERFLOW_FLOW_RIGHT
} FUEL_OVERFLOW_CHANNEL_t;

// implemented streaming operator for new type
inline std::ostream & operator<<(std::ostream & outstr, const FUEL_OVERFLOW_CHANNEL_t & val)
{
    outstr << (int)val;
    return outstr;
}
 
inline std::istream & operator>>(std::istream & instr, FUEL_OVERFLOW_CHANNEL_t & val)
{
       std::string temp;
       instr >> temp;
       val = (FUEL_OVERFLOW_CHANNEL_t)atoi(temp.c_str());
    return instr;
}