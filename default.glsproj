<?xml version="1.0" encoding="UTF-8"?>
<!-- 
    Adds per-project customizations to the GL Studio Editor

    This file is activated based on its position at or above GLS files in 
    the directory structure.

    This file is intended to be modified as necessary for a project's needs.     
-->    
<GLSProject>
    <!-- Set the folders where generated files are written -->
    <GeneratorOptions>
        <Generator id="Embedded C++"
                   codeOutputRoot="./Engineering/Generated/ES"
                   resourcesOutputRoot="./Engineering/Generated/ES"
                    />
        <Generator id="Desktop C++" 
                   codeOutputRoot="./Engineering/Generated/Desktop"
                   resourcesOutputRoot="./Engineering/Generated/Desktop"
                    />
    </GeneratorOptions>
    
    <!-- These scripts are called whenever GLS files in this project directory become active/inactive -->
    <Startup  script="./Engineering/ProjectSupport/ObjectCreateScripts/ProjectActivated.py"   />
    <Shutdown script="./Engineering/ProjectSupport/ObjectCreateScripts/ProjectDeactivated.py" />

    <!------ These items show up in the Project toolbar ------->

    <!-- Build and run scripts -->
    <ScriptButton name="Build" 
                  tooltip="Builds the deployment associated with the current generation mode" 
                  group="" icon="./Engineering/ProjectSupport/Icons/build_project.svg"
                  script="./Engineering/Deployments/Common/build_deployment_for_generation_mode.py"/>
                  
    <ScriptButton name="Clean"
                  tooltip="Cleans all generated and compiled output for the current generation mode" 
                  group="" icon="./Engineering/ProjectSupport/Icons/clean_project.svg" 
                  script="./Engineering/Deployments/Common/clean_deployment_for_generation_mode.py"/>
                  
    <ScriptButton name="Run"
                  tooltip="Builds and runs the deployment associated with the current generation mode" 
                  group="" icon="./Engineering/ProjectSupport/Icons/run_target.svg" 
                  script="./Engineering/Deployments/Common/build_and_run_deployment_for_generation_mode.py"/>
                  
                  
    <!-- Add this button with corresponding build script to deploy to your own target platform.
    <ScriptButton name="Run (Target)"
                  tooltip="Builds and runs the project on the target system" 
                  group="" icon="./Engineering/ProjectSupport/Icons/run_target.svg" 
                  script="./Engineering/Deployments/Target/build_and_run_from_editor.py"/>
    -->
    <!-- Utility -->
    <ScriptButton name="Art"
                  tooltip="Builds and runs the project on the target system" 
                  group="" icon="./Engineering/ProjectSupport/Icons/Stylized_Mona_Lisa.svg" 
                  script="./Engineering/ProjectSupport/ObjectCreateScripts/RevealDirectory_Art.py"/>
    
    <ScriptButton name="Copy Ref"
                  tooltip="Copies the selected objects to be pasted as an external reference"
                  group="" icon="./Engineering/ProjectSupport/Icons/disti_icon.svg" 
                  script="./Engineering/ProjectSupport/ObjectCreateScripts/CopyReferenceForExternal.py"/>

    <ScriptButton name="Paste Ref"
                  tooltip="Pastes the previously copied reference into a new external reference"
                  group="" icon="./Engineering/ProjectSupport/Icons/disti_icon.svg" 
                  script="./Engineering/ProjectSupport/ObjectCreateScripts/PasteExternalReference.py"/>

    <ScriptButton name=""
                  tooltip="Centers and resizes the orthogonal view of the main eye point, creating one if it does not exist"
                  group="" icon="./Engineering/ProjectSupport/Icons/center_eye.svg" 
                  script="./Engineering/ProjectSupport/ObjectCreateScripts/UpdateEyePoint.py"/>

    <ScriptButton name="" 
                  tooltip="Refreshes all catalyzers in the current document.  Necessary until local referenced object changes are detectable"
                  group="" icon="./Engineering/ProjectSupport/Icons/catalyzer_refresh.svg" 
                  script="./Engineering/ProjectSupport/ObjectCreateScripts/RefreshCatalyzers.py"/>
    
    <!-- These items go in the Toolbox -->
    
    <ScriptButton name="Reference PSD"
                  tooltip="Creates an object that gets its content from a PSD file"
                  group="External Reference" icon="./Engineering/ProjectSupport/Icons/disti_icon.svg"
                  script="./Engineering/ProjectSupport/ObjectCreateScripts/CreatePSDImporterObject.py"/>
                  
    <ScriptButton name="Reference DSI/ASE" 
                  tooltip="Creates an object that gets its content from an DSI/ASE file" 
                  group="External Reference" icon="./Engineering/ProjectSupport/Icons/disti_icon.svg"
                  script="./Engineering/ProjectSupport/ObjectCreateScripts/CreateASEImporterObject.py"/>
    
    <ScriptButton name="Reference Local Object(s)"  
                  tooltip="Creates a reference to locally selected objects"
                  group="External Reference" icon="./Engineering/ProjectSupport/Icons/disti_icon.svg"
                  script="./Engineering/ProjectSupport/ObjectCreateScripts/CreateLocalRef.py"/>
    
    <ScriptButton name="Reference External GLS File" 
                  tooltip="Prompts for an external GLS file and creates a reference to it"
                  group="Components" icon="./Engineering/ProjectSupport/Icons/disti_icon.svg"
                  script="./Engineering/ProjectSupport/ObjectCreateScripts/CreateExternalRefObject.py"/>
                  
    <ScriptButton name="9.png Poly"
                  tooltip="Creates a GlsNinePatch that is configured based on the edges and size of the .9.png file."
                  group="Polygons" icon="./Engineering/ProjectSupport/Icons/disti_icon.svg"
                  script="./Engineering/ProjectSupport/ObjectCreateScripts/CreateNineDotPngPoly.py"/>
                  
    <ScriptButton name="Convert to External GLS" 
                  tooltip="Moves selected objects to an external GLS file and creates a reference to them in its place" 
                  group="Components" icon="./Engineering/ProjectSupport/Icons/disti_icon.svg"
                  script="./Engineering/ProjectSupport/ObjectCreateScripts/ConvertToPrefab.py"/>
</GLSProject>