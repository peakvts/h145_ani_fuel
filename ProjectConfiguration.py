# This file is licensed under the MIT License.
# 
# Copyright (c) 2015 by The DiSTI Corporation.
# 
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
# 
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
# 
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.

""" Specifies project-wide settings. """

import sys
import os
import zlib # for crc32

class _ProjectDetails():
    def __init__(self):
        # The project file should be co-located with this configuraiton file
        self.projectDir = os.path.abspath(os.path.dirname( __file__ ))

        ###################################
        # Project specifics
        ###################################
        
        # Settings
        self.verboseBuildOutput = True
        
        ######## Directories #########
        self.deploymentsDir             = self.PathInProject( 'Engineering/Deployments')
        self.alternateClassBehaviorDir  = self.PathInProject( 'Engineering/ProjectSupport/AlternateClassBehaviors')
        self.additionalSourceDir        = self.PathInProject( 'Engineering/ProjectSupport/AdditionalSource')
        self.catalyzerScriptsDir        = self.PathInProject( 'Engineering/ProjectSupport/CatalyzerScripts')
        self.templatesDir               = self.PathInProject( 'Engineering/ProjectSupport/Templates')
        self.objectCreateScriptsDir     = self.PathInProject( 'Engineering/ProjectSupport/ObjectCreateScripts')
        self.iconsDir                   = self.PathInProject( 'Engineering/ProjectSupport/Icons')
        self.webGLBuildDir              = self.PathInProject( 'Engineering/Deployments/WebGL')
        self.desktopBuildDir            = self.PathInProject( 'Engineering/Deployments/Desktop')
        
        # Define which deployments are built and run by editor buttons
        # These are just defaults, more deployments can be created than generators exist
        self.generatorToDeploymentMap = {}
        self.generatorToDeploymentMap[ 'Desktop C++'  ] = self.desktopBuildDir
        self.generatorToDeploymentMap[ 'Embedded C++' ] = self.webGLBuildDir        
        
        # Art Assets
        self.threeDModelsDir = self.PathInProject( 'ArtAssets/3DModels')
        self.fontsDir        = self.PathInProject( 'ArtAssets/Fonts')
        self.texturesDir     = self.PathInProject( 'ArtAssets/Textures')

        # Output locations 
        self.generatedDir         = self.PathInProject( 'Engineering/Generated' )
        self.generatedTexturesDir = self.PathInProject( 'Engineering/Generated/Textures' )
        self.generatedDirES       = self.generatedDir + '/ES'
        self.generatedDirDesktop  = self.generatedDir + '/Desktop'
        
        # GL Studio Designs
        self.glsDir = self.PathInProject( 'Layout' )
        
        ######## Specific Files ######        
        # The top level GLS file that contains all content of the project
        self.topLevelGls = self.PathInProject( 'Layout/FrontPage.gls' )

        # Eye point for the main file
        self.defaultEyePointName = "mainView"
        
        # Project Web Portal
        class WebPortalData: pass
        self.webPortal = WebPortalData()
        # Choose an arbitrary port range to use for local http access to GL Studio.  
        # If a conflict with another project or application occurs, manually change this to a new value or range.
        # Details on know ports can be found here: https://en.wikipedia.org/wiki/List_of_TCP_and_UDP_port_numbers
        self.webPortal.port = self.PersistentUniquePort( 42000 )
        self.webPortal.pathMapping = {}
        self.webPortal.pathMapping["/builds/webgl/debug"]   = self.PathInProject( 'Engineering/Deployments/WebGL/dist/Debug' )
        self.webPortal.pathMapping["/builds/webgl/release"] = self.PathInProject( 'Engineering/Deployments/WebGL/dist/Release' )
        self.webPortal.webGLLaunchPath = 'http://localhost:'+str( self.webPortal.port )+'/builds/webgl/debug/index.html'      
        
    # Takes a relative path and joins it with this projects path
    def PathInProject( self, path ):
        return os.path.join(self.projectDir, path)
        
    # Convenience to make a path relative to a document
    def PathRelativeToDoc( self, doc, path ):
        return os.path.relpath( path, doc.GetDirname() )

    # Convenience to lookup an attribute and make a path relative to a document
    def RelativeToDoc( self, doc, attribName, filenameToJoin = None ):          
        dirName = os.path.relpath( getattr( self, attribName ), doc.GetDirname() )
        if filenameToJoin:
            return os.path.join( dirName, filenameToJoin )
        else:
            return dirName

    # Calculates a port number in the specified range which is based on the project directory
    # and so will remain constant while the project is in the same location.
    # Using the hashed directory name makes conflict with another project on the same
    # machine unlikely.
    def PersistentUniquePort( self, minPort, maxPort = None ):
        if not maxPort:
            maxPort = minPort + 1000
        return minPort + ( zlib.crc32( self.projectDir ) % ( maxPort - minPort ))
        
# Grab the latest details anytime this module is loaded (or reloaded)
current = _ProjectDetails()